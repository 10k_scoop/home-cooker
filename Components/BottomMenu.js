import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  AsyncStorage,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { MaterialIcons, Ionicons,FontAwesome5 } from "@expo/vector-icons";

export default function BottomMenu(props) {
  const onHome = async () => {
    const cookerData = await AsyncStorage.getItem("userData");

    if (JSON.parse(cookerData)?.profil == "COOKER") {
      props?.navigation.navigate("CookerMeal");
    } else if (JSON.parse(cookerData)?.profil == "CUSTOMER") {
      props?.navigation.navigate("MealList");
    } else {
    }
  };

  return (
    <View style={styles.BottomMenu}>
      <TouchableOpacity style={styles.IconCircle} onPress={onHome}>
        {/* <Image
          style={{ width: "100%", height: "100%" }}
          source={require("../assets/Chef.png")}
          resizeMode="contain"
        /> */}
        <Ionicons name="fast-food-sharp" size={rf(30)} color="#fff" />
      </TouchableOpacity>
      <TouchableOpacity>
        <MaterialIcons name="search" size={rf(30)} color="#fff" />
      </TouchableOpacity>
      <TouchableOpacity>
        <Ionicons name="heart-outline" size={rf(30)} color="#fff" />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.IconCircle}
        onPress={() =>
          props.userProfile == "CUSTOMER"
            ? props.navigation.navigate("CustomerDashboard")
            : props.navigation.navigate("CookerDashboard")
        }
      >
       <FontAwesome5 name="user-circle" size={rf(30)} color="#fff" />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  BottomMenu: {
    width: wp("100%"),
    height: Platform.OS == "ios" ? hp("8%") : hp("7%"),
    backgroundColor: "#5E0D13",
    position: "absolute",
    bottom: 0,
    borderTopLeftRadius: 7,
    borderTopRightRadius: 7,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingBottom: Platform.OS == "ios" ? 15 : 5,
  },
  IconCircle: {
    width: hp("5%"),
    height: hp("5%"),
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#fff",
    overflow: "hidden",
  },
});
