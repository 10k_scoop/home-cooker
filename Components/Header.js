import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf, RFValue } from "react-native-responsive-fontsize";
import {
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { AntDesign } from "@expo/vector-icons";

export default function Header({ navigation, OnBackPress, Title }) {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={OnBackPress} style={styles.OnBackPress}>
        <AntDesign name="back" size={rf(17)} color="black" />
      </TouchableOpacity>
      <Text style={{ fontSize: rf(36),color:"#5E0D13" }}>{Title}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: hp("20%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    paddingTop: hp("5%"),
    justifyContent: "flex-end",
  },
  OnBackPress:{
    bottom:hp('4%')
  }
});
