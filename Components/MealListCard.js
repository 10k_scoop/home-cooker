import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  Platform,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { EvilIcons, Entypo } from "@expo/vector-icons";
import { getDistance } from "geolib";
export default function MealListCard(props) {
  var dis;
  if (props.data?.geolocation) {
     dis = getDistance(
      {
        latitude: props.location?.coords?.latitude,
        longitude: props.location?.coords?.longitude,
      },
      {
        latitude: props.data?.geolocation?.latitude,
        longitude: props.data?.geolocation?.longitude,
      }
    );
  }

  var base64Image =
    "data:image/png;base64," + props?.data?.meal?.photos[0]?.image?.data;
  var img =
    "https://images.unsplash.com/photo-1519708227418-c8fd9a32b7a2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bWVhbHxlbnwwfHwwfHw%3D&w=1000&q=80";

  return (
    <TouchableOpacity style={styles.container} onPress={props.onpress}>
      {props?.type == "cooker" && (
        <TouchableOpacity
          style={styles.removeBtn}
          onPress={props.onRemovePress}
        >
          <Text style={styles.removeBtnText}>X</Text>
        </TouchableOpacity>
      )}
      <View style={styles.Img}>
        <ImageBackground
          style={{ width: "100%", height: "100%" }}
          // source={{ uri:props.data?.meal?.photos[0]?.image!=null?props.data?.meal?.photos[0]?.image:img}}
          source={{ uri: base64Image }}
          resizeMode="cover"
        >
          <View style={styles.Icon}>
            <TouchableOpacity>
              <EvilIcons name="heart" size={rf(30)} color="#fff" />
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </View>
      <View style={styles.discription}>
        <View style={styles.details}>
          <Text style={styles.Font1}>{props.data?.meal.title}</Text>
          <Text style={styles.Font2}>
            {" "}
            à {props.data?.geolocation?Math.floor(dis / 1000):0} km - {props.data?.meal?.categories[0]}
          </Text>
          <View style={styles.InnerDetails}>
            <Text style={{ fontSize: rf(16) }}>
              {props.data?.meal?.cooker?.username}
            </Text>
            <Entypo name="star" size={rf(18)} color="#F4D639" />
            <Text style={{ fontSize: rf(16) }}>
              {props.data?.meal?.cooker.totalRating.totalRatings}
            </Text>
          </View>
        </View>
        <View style={styles.Bar}>
          <Text style={{ fontSize: rf(10), marginBottom: 4 }}>
            {/* {props?.hours > props.data?.meal.timeForConsumption
              ? "Expired"
              : "reste " +
                eval(props.data?.meal.timeForConsumption - props?.hours) +
                " Hour"} */}
            reste {props.data?.meal.timeForConsumption} Hour
          </Text>
          <View style={styles.BarLine}>
            <View
              style={[styles.InnerBarLine, { width: 100 - props?.MRT + "%" }]}
            ></View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("95%"),
    height: hp("25%"),
    borderRadius: 10,
    backgroundColor: "#ECECEC",
    overflow: "hidden",
    alignItems: "center",
    marginBottom: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  Img: {
    width: "100%",
    height: "65%",
    borderRadius: 10,
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  Icon: {
    width: "100%",
    height: "40%",
    justifyContent: "center",
    alignItems: "flex-end",
    paddingHorizontal: "3%",
  },
  discription: {
    width: "100%",
    height: "35%",
    flexDirection: "row",
  },
  details: {
    width: "60%",
    height: "100%",
    justifyContent: "space-evenly",
    paddingHorizontal: 4,
  },
  Font1: {
    fontSize: rf(16),
    fontWeight: "700",
  },
  Font2: {
    fontSize: rf(10),
    color: "#222",
    fontWeight: "normal",
  },
  InnerDetails: {
    flexDirection: "row",
    width: "50%",
    justifyContent: "space-evenly",
  },
  Bar: {
    width: "40%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  BarLine: {
    width: "80%",
    height: "15%",
    borderWidth: 0.6,
    borderRadius: 100,
  },
  InnerBarLine: {
    height: "100%",
    backgroundColor: "#74DB63",
    borderRadius: 100,
    borderWidth: 0.8,
  },
  removeBtn: {
    width: 25,
    height: 25,
    backgroundColor: "red",
    borderRadius: 100,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    left: 0,
    zIndex: 9999999,
  },
  removeBtnText: {
    fontSize: 15,
    color: "white",
    fontWeight: "bold",
  },
});
