import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
export default function Button({ onPress, BtnTxt, OnCLick }) {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Text style={{ fontSize: rf(18), color: "#fff",fontWeight:"700" }}>{BtnTxt}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    height: hp("7%"),
    width: wp("90%"),
    justifyContent: "center",
    backgroundColor: "#5E0D13",
    borderRadius: 5,
    alignItems: "center",
  },
});
