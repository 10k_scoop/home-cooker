import React, { useEffect, useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  FontAwesome5,
  AntDesign,
  Entypo,
  Ionicons,
  FontAwesome,
} from "@expo/vector-icons";
import { connect } from "react-redux";

const Header1 = (props) => {
  //var ordersDetails = props?.orderDetails?.content[0]?.orders;
  const [ordersCount, setOrdersCount] = useState(0);

  useEffect(() => {
    if (props.get_meals_by_cooker != "") {
      let allOD = JSON.parse(props.get_meals_by_cooker)?.content[0]?.orders;
      if (props?.path == "cooker") {
        allOD?.map((item, index) => {
          console.log(item?.stateOrder)
          if (item?.stateOrder == "CREATED") {
            setOrdersCount(ordersCount + 1);
          }
        });
      }
    }
  }, []);

  return (
    <View style={styles.Header}>
      <TouchableOpacity style={styles.HeaderDiscription}>
        <Text style={styles.Font1}>
          {props?.locationName.length < 1
            ? "loading.."
            : "now " + props?.locationName[0]?.name}{" "}
        </Text>
        <Ionicons name="location" size={rf(20)} color="#5E0D13" />
      </TouchableOpacity>
      {props.refresh && (
        <TouchableOpacity onPress={props.onRefreshPress}>
          <FontAwesome
            name="refresh"
            size={rf(20)}
            color="black"
            style={{ right: 15 }}
          />
        </TouchableOpacity>
      )}
      <TouchableOpacity onPress={() => props.onCartPress()}>
        {props.orders >= 1 && (
          <View style={styles.orderCounter}>
            <Text style={styles.orderCounterText}>
              {ordersCount > 0 ? ordersCount : 0}
            </Text>
          </View>
        )}
        <AntDesign
          name="shoppingcart"
          size={rf(25)}
          color="black"
          style={{ right: 15 }}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  Header: {
    width: wp("100%"),
    height: hp("11%"),
    justifyContent: "space-between",
    alignItems: "flex-end",
    flexDirection: "row",
    marginBottom: 10,
    paddingHorizontal: "3%",
  },
  HeaderDiscription: {
    width: "65%",
    height: "50%",
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "flex-start",
  },
  Font1: {
    fontSize: rf(14),
    color: "#222",
    marginRight: 10,
  },
  Font2: {
    fontSize: rf(18),
    fontWeight: "700",
    color: "#222",
  },
  orderCounter: {
    width: 20,
    height: 20,
    borderRadius: 100,
    backgroundColor: "red",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    right: 5,
    top: -5,
    zIndex: 9999,
  },
  orderCounterText: {
    fontSize: rf(9),
    fontWeight: "700",
    color: "#fff",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_oauth_token: state.main.get_user_oauth_token,
  get_user_details: state.main.get_user_details,
  get_meals_by_cooker: state.main.get_meals_by_cooker,
});
export default connect(mapStateToProps, {})(Header1);
