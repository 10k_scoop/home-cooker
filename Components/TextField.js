import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { StyleSheet, Text, TextInput, View } from "react-native";
export default function TextField({
  Txt,
  keyboardType,
  placeholder,
  onChangeText,
  secureText,
}) {
  return (
    <View style={styles.container}>
      <View style={styles.FieldWrapper}>
        <TextInput
          keyboardType={keyboardType}
          style={{
            height: "100%",
            width: "100%",
            fontSize: rf(13),
            fontWeight: "600",
          }}
          placeholder={placeholder}
          onChangeText={(val)=>onChangeText(val)}
          secureTextEntry={secureText}
          placeholderTextColor="grey"
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "space-evenly",
    height: hp("9%"),
  },
  FieldWrapper: {
    borderWidth: 1,
    paddingHorizontal: "5%",
    justifyContent: "center",
    width: wp("90%"),
    height: hp("7%"),
    borderColor: "#e5e5e5",
    borderRadius: 10,
  },
  TxtWrapper: {
    flex: 0.3,
    justifyContent: "center",
  },
});
