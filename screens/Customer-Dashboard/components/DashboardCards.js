import React, { useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import {
  MaterialCommunityIcons,
  MaterialIcons,
  Ionicons,
  SimpleLineIcons,
  Zocial,
  Entypo,
} from "@expo/vector-icons";

export default function DashboardCards(props) {
  return (
    <TouchableOpacity style={styles.Card} onPress={props.onClick}>
      {props.Command ? (
        <MaterialCommunityIcons
          name="message-text-outline"
          size={rf(26)}
          color="#fff"
        />
      ) : null}
      {props.profile ? (
        <MaterialCommunityIcons
          name="account-search-outline"
          size={26}
          color="#fff"
        />
      ) : null}
      {props.medal ? <Entypo name="medal" size={20} color="#fff" /> : null}
      {props.payment ? (
        <MaterialIcons name="payment" size={26} color="#fff" />
      ) : null}
      {props.location ? (
        <Ionicons name="ios-location-outline" size={26} color="#fff" />
      ) : null}
      {props.Msgs ? <Zocial name="email" size={26} color="#fff" /> : null}
      {props.Invite ? (
        <MaterialCommunityIcons name="human-male-male" size={26} color="#fff" />
      ) : null}
      {props.Question ? (
        <SimpleLineIcons name="question" size={26} color="#fff" />
      ) : null}
      {props.upload ? (
        <MaterialCommunityIcons
          name="file-upload-outline"
          size={26}
          color="#fff"
        />
      ) : null}
      <Text style={styles.font}>{props.title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  Card: {
    width: wp("100%"),
    height: hp("6%"),
    backgroundColor: "#5E0D13",
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: wp("4%"),
    borderBottomWidth: 1,
    borderColor: "#fff",
  },
  font: {
    fontSize: rf(16),
    fontWeight: "400",
    color: "#fff",
    marginLeft: "2%",
  },
});
