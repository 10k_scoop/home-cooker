import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import DashboardCards from "./components/DashboardCards";
import BottomMenu from "../../Components/BottomMenu";
import { connect } from "react-redux";
import { logout } from "../../state-management/actions/auth/authActions";

const CustomerDashboard = (props) => {
  return (
    <View style={styles.Container}>
      <View style={styles.title}>
        <Text style={styles.titleText}>Dashboard</Text>
      </View>
      <DashboardCards
        title="Mes commandes"
        Command
        onClick={() => props.navigation.navigate("CustomerCommandes")}
      />
      <View style={styles.EmptyView}></View>
      <DashboardCards
        title="Mes informations"
        profile
        onClick={() => props.navigation.navigate("CustomerInformation")}
      />
      <DashboardCards title="Mes Récompenses" medal />
      <DashboardCards
        title="Mes Moyens de paiements"
        payment
        onClick={() => props.navigation.navigate("CustomerPaiement")}
      />
      <DashboardCards
        title="Mes adresses"
        location
        onClick={() => props.navigation.navigate("CustomerAddress1")}
      />
      <View style={[styles.EmptyView, { height: hp("1%") }]}></View>
      <DashboardCards title="Mes Messages" Msgs />
      <DashboardCards
        title="Mes bons et credits"
        Command
        onClick={() => props.navigation.navigate("CustomerCredit")}
      />
      <DashboardCards
        title="Inviter des amis"
        Invite
        onClick={() => props.navigation.navigate("CustomerInviter")}
      />
      <View style={styles.EmptyView}></View>
      <DashboardCards
        title="FAQ"
        Question
        onClick={() => props.navigation.navigate("CustomerFAQ")}
      />
      <DashboardCards
        title="Déconnexion"
        upload
        onClick={() =>
          {
            props.logout()
            props.navigation.navigate("ChooseOption")
          } 
        }
      />
      <DashboardCards
        title="À propos"
        onClick={() => props.navigation.navigate("APropos")}
      />
      <BottomMenu navigation={props.navigation} userProfile={JSON.parse(props.get_user_details)?.profil} />
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  title: {
    width: wp("100%"),
    height: hp("15%"),
    justifyContent: "flex-end",
    paddingHorizontal: wp("4%"),
    paddingVertical: hp("4%"),
  },
  titleText: {
    fontSize: rf(22),
    fontWeight: "400",
    color: "#5E0D13",
  },
  EmptyView: {
    width: wp("100%"),
    height: hp("4%"),
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details:state.main.get_user_details
});
export default connect(mapStateToProps, { logout })(CustomerDashboard);
