import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from "@expo/vector-icons";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
export default function ForgetPassword({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.BackIcon}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <AntDesign name="back" size={rf(17)} color="black" />
        </TouchableOpacity>
      </View>
      <View style={styles.title}>
        <Text style={styles.titleText}>Forget password</Text>
      </View>
      <View style={styles.EmailWrapper}>
        <Text style={styles.EmailText}>e-mail</Text>
        <TextInput
          style={styles.textfield}
          placeholder="jane"
          placeholderTextColor="#5E0D13"
        />
      </View>
      <View style={styles.BtnWrapper}>
        <TouchableOpacity style={styles.Btn}>
          <Text style={styles.BtnText}>Validate & put online</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  BackIcon: {
    height: hp("10%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "flex-end",
  },
  title: {
    width: wp("90%"),
    height: hp("10%"),
    justifyContent: "center",
  },
  titleText: {
    fontSize: rf(22),
    fontWeight: "400",
    color: "#5E0D13",
  },
  EmailWrapper: {
    width: wp("90%"),
    height: hp("12%"),
    justifyContent: "space-around",
  },
  textfield: {
    width: "100%",
    height: "50%",
    borderWidth: 1.5,
    borderColor: "#5E0D13",
    fontSize: rf(14),
    paddingHorizontal: "2%",
  },
  EmailText: {
    fontSize: rf(14),
    color: "#5E0D13",
  },
  BtnWrapper: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: hp("5%"),
  },
  Btn: {
    width: wp("90%"),
    height: hp("7%"),
    borderRadius: 10,
    backgroundColor: "#5E0D13",
    alignItems: "center",
    justifyContent: "center",
  },
  BtnText: {
    fontSize: rf(14),
    fontWeight: "700",
    color: "#fff",
  },
});
