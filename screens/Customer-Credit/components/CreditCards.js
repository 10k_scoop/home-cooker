import React, { useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";

export default function CreditCards(props) {
  return (
    <View style={styles.Card} onPress={props.onClick}>
      <View style={styles.Img}>
        <Image
          source={props.img}
          style={{ width: "100%", height: "60%" }}
          resizeMode="contain"
        />
      </View>
      <View style={styles.Detail}>
        <Text style={styles.font1}>{props.title}</Text>
        <Text style={styles.font2}>Valide jusqu’au 16/05/2021</Text>
      </View>
      <View style={styles.Amount}>
        <Text style={styles.font3}>{props.amount}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  Card: {
    width: wp("90%"),
    height: hp("7%"),
    borderBottomWidth: 1,
    borderColor: "#5E0D13",
    flexDirection: "row",
  },
  Img: {
    width: "15%",
    height: "100%",
    justifyContent: "center",
  },
  Detail: {
    width: "65%",
    height: "100%",
    justifyContent: "center",
  },
  Amount: {
    width: "20%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  font1: {
    fontSize: rf(16),
    color: "#5E0D13",
    fontWeight: "700",
  },
  font2: {
    fontSize: rf(12),
    color: "#815256",
  },
  font3: {
    fontSize: rf(16),
    color: "#5E0D13",
    fontWeight: "400",
  },
});
