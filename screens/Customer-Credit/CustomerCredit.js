import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import BottomMenu from "../../Components/BottomMenu";
import CreditCards from "./components/CreditCards";

export default function CustomerCredit({ navigation }) {
  return (
    <View style={styles.Container}>
      <View style={styles.title}>
        <Text style={styles.titleText}>Bons et credits</Text>
      </View>
      <View style={styles.SearchRow}>
        <View style={styles.SearchBox}>
          <TextInput style={styles.TextField} />
          <Text style={styles.SearchText}>Le code entré n’est pas valide</Text>
        </View>
        <TouchableOpacity style={styles.SearchBtn}>
          <Text style={styles.SearchBtnText}>Ajouter</Text>
        </TouchableOpacity>
      </View>
      <ScrollView>
        <View style={{ alignItems: "center", marginBottom: hp("10%") }}>
          <View style={styles.CardsTitle}>
            <Text style={styles.font1}>Actif</Text>
          </View>
          <CreditCards
            img={require("../../assets/MovieTicket.png")}
            title="Bon"
            amount="2,67 €"
          />
          <CreditCards
            img={require("../../assets/EuroMoney.png")}
            title="Credit"
            amount="3,60 €"
          />
          <View style={styles.CardsTitle}>
            <Text style={styles.font1}>Inactif</Text>
          </View>
          <CreditCards
            img={require("../../assets/MovieTicket.png")}
            title="Bon"
            amount="2,67 €"
          />
          <CreditCards
            img={require("../../assets/MovieTicket.png")}
            title="Bon"
            amount="2,67 €"
          />
          <CreditCards
            img={require("../../assets/EuroMoney.png")}
            title="Credit"
            amount="3,60 €"
          />
          <CreditCards
            img={require("../../assets/MovieTicket.png")}
            title="Bon"
            amount="2,67 €"
          />
          <CreditCards
            img={require("../../assets/EuroMoney.png")}
            title="Credit"
            amount="3,60 €"
          />
        </View>
      </ScrollView>
      <BottomMenu />
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  title: {
    width: wp("100%"),
    height: hp("15%"),
    justifyContent: "flex-end",
    paddingHorizontal: wp("4%"),
    paddingVertical: hp("4%"),
  },
  titleText: {
    fontSize: rf(26),
    fontWeight: "400",
    color: "#5E0D13",
  },
  SearchRow: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
  SearchBox: {
    width: "70%",
    height: "60%",
    borderWidth: 0.5,
    backgroundColor: "#e5e5e5",
  },
  SearchBtn: {
    width: "18%",
    height: "50%",
    borderWidth: 1.5,
    borderRadius: 6,
    backgroundColor: "#FF1E00",
    borderColor: "#5E0D13",
    alignItems: "center",
    justifyContent: "center",
  },
  SearchBtnText: {
    fontSize: rf(15),
    color: "#fff",
    fontWeight: "700",
  },
  TextField: {
    width: "100%",
    height: "100%",
    paddingHorizontal: "2%",
  },
  CardsTitle: {
    width: wp("100%"),
    height: hp("8%"),
    justifyContent: "flex-end",
    paddingHorizontal: "4%",
    paddingVertical: "4%",
    marginTop: 5,
  },
  font1: {
    fontSize: rf(20),
    fontWeight: "400",
    color: "#5E0D13",
  },
  SearchText: {
    fontSize: rf(12),
    marginTop: 3,
    color: "#FF1E00",
  },
});
