import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { MaterialIcons } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Image,
} from "react-native";
export default function CustomerPaypal({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.BackIcon}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <AntDesign name="back" size={rf(17)} color="black" />
        </TouchableOpacity>
      </View>
      <View style={styles.PaypalIconWrapper}>
        <View style={styles.PaypalIconBody}>
          <Image
            style={{ height: "100%", width: "100%" }}
            source={require("../../assets/Paypal.jpeg")}
            resizeMode="contain"
          />
        </View>
      </View>
      <View style={styles.BtnWrapper}>
        <TouchableOpacity
          style={styles.BtnBody}
          onPress={() => navigation.navigate("CustomrerComfirmation")}
        >
          <Text style={{ fontWeight: "500", fontSize: rf(16), color: "#fff" }}>
            Valider le paiement et accéder à PayPal
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  BackIcon: {
    height: hp("10%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "flex-end",
  },
  PaypalIconWrapper: {
    height: hp("30%"),
    width: wp("100%"),
    justifyContent: "flex-end",
    alignItems: "center",
  },
  PaypalIconBody: {
    height: "60%",
    width: "50%",
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
  },
  BtnWrapper: {
    height: hp("20%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
  },
  BtnBody: {
    height: hp("6%"),
    width: wp("90%"),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#DCA007",
    borderRadius: 100,
  },
});
