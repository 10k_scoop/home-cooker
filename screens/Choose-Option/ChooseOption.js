import React, { useEffect,useState } from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  View,
} from "react-native";
import { connect } from "react-redux";
import { AsyncStorage } from "react-native";
import { getUserOAuthToken } from "../../state-management/actions/auth/authActions";

const ChooseOption = (props) => {
  
  const [loading, setLoading] = useState(false);


  useEffect(() => {
    (async () => {
      const value = await AsyncStorage.getItem("userData");
      if (value !== null) {
        setLoading(true)
        let data = {
          username: JSON.parse(value)?.username,
          password: JSON.parse(value)?.password,
        };
        await props.getUserOAuthToken(data, setLoading);
        if (JSON.parse(value)?.profil == "CUSTOMER") {
          props.navigation.reset({
            index: 0,
            routes: [{ name: "MealList" }],
          });
        } else {
          props.navigation.reset({
            index: 0,
            routes: [{ name: "CookerMeal" }],
          });
        }
      }
    })().catch((err) => {
      console.log(err);
    });
  }, [props]);
  

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator color="black" size="large" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <ImageBackground
        style={styles.ImageWrapper}
        source={require("../../assets/CookPic.png")}
      >
        <View style={styles.ImageLogo}>
          <Image
            style={{ width: "60%", height: "60%" }}
            source={require("../../assets/LogoPic.png")}
          />
        </View>
      </ImageBackground>
      <View style={styles.BtnWrapper}>
        <TouchableOpacity
          onPress={() =>
            props.navigation.navigate("LoggedOut", { role: "cooker" })
          }
          style={styles.LoginBtn}
        >
          <Text style={{ fontSize: rf(13), color: "#222" }}>Cooker</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            props.navigation.navigate("LoggedOut", { role: "customer" })
          }
          style={styles.RegisterBtn}
        >
          <Text style={{ fontSize: rf(13), color: "#fff" }}>Customer</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  ImageWrapper: {
    height: hp("100%"),
    width: wp("100%"),
    justifyContent: "center",
  },

  BtnWrapper: {
    height: hp("12%"),
    width: wp("100%"),
    position: "absolute",
    bottom: 0,
    paddingHorizontal: wp("5%"),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#fff",
    borderTopStartRadius: 20,
    borderTopEndRadius: 20,
  },
  LoginBtn: {
    height: "50%",
    width: "45%",
    borderRadius: 10,
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#5E0D13",
  },
  RegisterBtn: {
    height: "50%",
    width: "45%",
    borderRadius: 10,
    backgroundColor: "#FF1E00",
    justifyContent: "center",
    alignItems: "center",
  },
  ImageLogo: {
    width: "100%",
    height: "60%",
    alignItems: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_oauth_token: state.main.get_user_oauth_token,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { getUserOAuthToken })(ChooseOption);
