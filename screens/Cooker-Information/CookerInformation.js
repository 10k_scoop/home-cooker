import React, { useState } from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import Header from "../../Components/Header";
import TextField from "../../Components/TextField";
import Button from "../../Components/Button";
import { Picker } from "@react-native-picker/picker";

export default function CookerInformation(props) {
  const [username, setUsername] = useState("");
  const [age, setAge] = useState("");
  const [gender, setGender] = useState("MALE");

  const data = props.route.params;

  const onNext = () => {
    if (username == "" || age == "") {
      alert("Fill all fields");
    } else {
      props.navigation.navigate("CookerLocation", {
        email: data.email,
        pass: data.pass,
        age: age,
        gender: gender,
        username: username,
        oAuthToken: data.oAuthToken,
      });
    }
  };

  return (
    <View style={styles.container}>
      <Header
        Title="Information"
        OnBackPress={() => props.navigation.goBack()}
      />
      <ScrollView>
        <View style={{ height: hp("9%") * 5.4 }}>
          <View style={[styles.inputWrapper, { marginTop: hp("2%") }]}>
            <Text style={styles.label}>Username</Text>
            <TextField
              Txt="Username"
              placeholder="John_234"
              onChangeText={(val) => setUsername(val)}
            />
          </View>
          <View style={styles.inputWrapper}>
            <Text style={styles.label}>Age</Text>
            <TextField
              Txt="Age"
              placeholder="23"
              onChangeText={(val) => setAge(val)}
            />
          </View>
          <View style={styles.inputWrapper}>
            <Text style={styles.label}>Sex</Text>
            <View style={styles.picker}>
              <Picker
                selectedValue={gender}
                onValueChange={(itemValue, itemIndex) => setGender(itemValue)}
                itemStyle={{ fontSize: rf(13) }}
              >
                <Picker.Item label="Male" value="MALE" />
                <Picker.Item label="Female" value="FEMALE" />
              </Picker>
            </View>
          </View>
        </View>
        <View style={styles.BtnWrapper}>
          <Button BtnTxt="Next" onPress={onNext} />
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  BtnWrapper: {
    height: hp("10%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "flex-end",
    alignItems: "center",
  },
  inputWrapper: {
    width: wp("100%"),
    marginVertical: hp("0.3%"),
  },
  label: {
    fontSize: rf(12),
    fontWeight: "500",
    paddingHorizontal: wp("5%"),
    left: 5,
  },
  picker: {
    borderWidth: 1,
    paddingHorizontal: "5%",
    justifyContent: "center",
    width: wp("90%"),
    height: hp("7%"),
    marginHorizontal: wp("5%"),
    overflow: "hidden",
    marginTop: 10,
    borderColor: "#e5e5e5",
    borderRadius: 10,
  },
});
