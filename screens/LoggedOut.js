import React, { useState } from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  ActivityIndicator,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { register_User } from "../state-management/actions/auth/authActions";
import { connect } from "react-redux";

const LoggedOut = (props) => {
  const [loading, setLoading] = useState(false);

  const onRegister = () => {
    props.navigation.navigate(
      props.route.params.role == "cooker" ? "CookerRegister" : "Register1"
    );
  };

  if (loading) {
    return (
      <View
        style={{ flex: 1, justifyContent: "center", alignContent: "center" }}
      >
        <ActivityIndicator size="large" color="black" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <ImageBackground
        style={styles.ImageWrapper}
        source={require("../assets/CookPic.png")}
      >
        <View style={styles.ImageLogo}>
          <Image
            style={{ width: "60%", height: "60%" }}
            source={require("../assets/LogoPic.png")}
          />
        </View>
      </ImageBackground>
      <View style={styles.BtnWrapper}>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("Login")}
          style={styles.LoginBtn}
        >
          <Text style={{ fontSize: rf(13), color: "#222" }}>LOG IN</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={onRegister} style={styles.RegisterBtn}>
          <Text style={{ fontSize: rf(13), color: "#fff" }}>REGISTER</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  ImageWrapper: {
    height: hp("100%"),
    width: wp("100%"),
    justifyContent: "center",
  },

  BtnWrapper: {
    height: hp("12%"),
    width: wp("100%"),
    position: "absolute",
    bottom: 0,
    paddingHorizontal: wp("5%"),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#fff",
    borderTopStartRadius: 20,
    borderTopEndRadius: 20,
  },
  LoginBtn: {
    height: "50%",
    width: "45%",
    borderRadius: 10,
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#5E0D13",
  },
  RegisterBtn: {
    height: "50%",
    width: "45%",
    borderRadius: 10,
    backgroundColor: "#FF1E00",
    justifyContent: "center",
    alignItems: "center",
  },
  ImageLogo: {
    width: "100%",
    height: "60%",
    alignItems: "center",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  signup_res: state.main.signup_res,
});
export default connect(mapStateToProps, { register_User })(LoggedOut);
