import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { MaterialIcons } from "@expo/vector-icons";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
export default function CustomrerComfirmation({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.Header}>
        <Text style={{ fontSize: rf(22), color: "#222", marginBottom: 5 }}>
          Confirmation
        </Text>
      </View>
      <View style={styles.ConfimationTxtWrapper}>
        <View style={{ alignItems: "center" }}>
          <Text
            style={{ fontSize: rf(22), color: "#C22B2B", fontWeight: "400" }}
          >
            Merci Jane
          </Text>
          <Text
            style={{ fontSize: rf(18), color: "#C22B2B", fontWeight: "400" }}
          >
            Votre commande a été enregistrée
          </Text>
        </View>
      </View>
      <View style={styles.ConfirmationIconWrapper}>
        <View style={styles.IconBody}>
          <MaterialIcons name="done-outline" size={rf(30)} color="#CCFF90" />
        </View>
      </View>
      <View style={styles.ComentsWrapper}>
        <TouchableOpacity style={styles.ComentsBody}>
          <Text
            style={{ fontSize: rf(18), color: "#111010", fontWeight: "500" }}
          >
            Mes commandes
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  Header: {
    height: hp("15%"),
    width: wp("100%"),
    alignItems: "center",
    justifyContent: "flex-end",
    borderBottomWidth: 1,
    borderBottomColor: "#ACADAC",
  },
  ConfimationTxtWrapper: {
    height: hp("20%"),
    width: wp("100%"),
    alignItems: "center",
    justifyContent: "flex-end",
  },
  ConfirmationIconWrapper: {
    height: hp("22%"),
    width: wp("100%"),
    alignItems: "center",
    justifyContent: "center",
  },
  IconBody: {
    height: wp("22%"),
    width: wp("22%"),
    borderRadius: 100,
    backgroundColor: "#4CAF50",
    justifyContent: "center",
    alignItems: "center",
  },
  ComentsWrapper: {
    height: hp("13%"),
    width: wp("100%"),
    justifyContent: "center",
    alignItems: "center",
  },
  ComentsBody: {
    height: hp("6%"),
    width: wp("60%"),
    borderRadius: 100,
    borderWidth: 1,
    borderColor: "#000000",
    justifyContent: "center",
    alignItems: "center",
  },
});
