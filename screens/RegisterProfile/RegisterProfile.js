import React, { useState, useEffect } from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  Platform,
} from "react-native";
import Header from "../../Components/Header";
import Button from "../../Components/Button";
import { Entypo } from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";

export default function RegisterProfile({ navigation, route }) {
  const [image, setImage] = useState(null);
  const [profile, setProfile] = useState(null);

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const { status } =
          await ImagePicker.requestMediaLibraryPermissionsAsync();
      }
    })();
  }, []);
  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.PHOTO,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
      setProfile(result)
    }

  };
  const data = route.params;
  const onNext = () => {
    if (image) {
      navigation.navigate("CustomerRegulation", {
        email: data.email,
        password: data.password,
        username: data.username,
        age: data.age,
        sex: data.sex,
        image: image,
        profileImg: profile,
        phoneNumber: data.number,
        oAuthToken: data.oAuthToken,
        profil: "CUSTOMER",
        address: {
          number: data.number,
          street: data.street,
          zipCode: parseInt(data.zipCode),
          country: data.country,
          state: 0,
        },
        totalRating: {
          totalComments: 0,
          totalRatings: 0,
          avgScore: 0.0,
        },
        meals: null,
        favoriteMeals: null,
      });
    }
  };

  return (
    <View style={styles.container}>
      <Header Title="Photo" OnBackPress={() => navigation.goBack()} />
      <View style={{ height: hp("9%") * 5.3 }}>
        <View style={styles.TextWrapper}>
          <Text style={{ fontSize: rf(14), fontWeight: "600" }}>
            Profile Picture
          </Text>
        </View>
        <View style={styles.ProfilePicWrapper}>
          <TouchableOpacity style={styles.ProfilePicBody} onPress={pickImage}>
            {image && (
              <Image
                source={{ uri: image }}
                style={{ width: "100%", height: "100%" }}
                resizeMode="cover"
              />
            )}
            <Entypo
              name="camera"
              size={rf(35)}
              color="black"
              style={styles.cameraIcon}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.BtnWrapper}>
        <Button BtnTxt="FINISH" onPress={onNext} />
      </View>
      <View style={styles.BotomTxtWrapper}>
        <Text style={{ fontSize: rf(14), color: "#C90002" }}>
          Do you want to become a cooker and make money?
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  BtnWrapper: {
    height: hp("15%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
    alignItems: "center",
  },
  ProfilePicWrapper: {
    height: hp("30%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
    alignItems: "center",
  },
  TextWrapper: {
    height: hp("5%"),
    justifyContent: "center",
    paddingHorizontal: wp("5%"),
  },
  ProfilePicBody: {
    height: hp("25%"),
    width: hp("25%"),
    borderRadius: hp("25%") / 2,
    backgroundColor: "#C4C4C4",
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
  },
  BotomTxtWrapper: {
    width: wp("100%"),
    paddingHorizontal: wp("3%"),
    justifyContent: "center",
    alignItems: "center",
  },
  cameraIcon: {
    position: "absolute",
  },
});
