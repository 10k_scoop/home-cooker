import React, { useState, useEffect } from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  ActivityIndicator,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import Header from "../../Components/Header";
import TextField from "../../Components/TextField";
import Button from "../../Components/Button";
import { connect } from "react-redux";
import { getOAuthToken } from "../../state-management/actions/auth/authActions";

const Register1 = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confrimPassword, setConfirmPassword] = useState("");
  const [token, setToken] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    props.getOAuthToken(setLoading);
  }, []);


  useEffect(() => {
    if (props.get_oauth_token?.access_token) {
      setToken(props.get_oauth_token?.access_token);
      console.log(props.get_oauth_token?.access_token);
    } else {
      console.log(props.errors);
    }
  }, [props]);



  const onNext = () => {
    if (email != "" && password != "" && confrimPassword != "") {
      if (password == confrimPassword) {
        if (password.length <= 7) {
          alert("Password must be 8 characters long");
        } else {
          props.navigation.navigate("RegisterBasicInfo", {
            email: email,
            password: password,
            oAuthToken: token,
          });
        }
      } else {
        alert("Password does not match");
      }
    } else {
      alert("fill all details");
    }
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="#222" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Header Title="Welcome" OnBackPress={() => props.navigation.goBack()} />
      <ScrollView style={{ marginTop: 15 }}>
        <View style={{ height: hp("9%") * 5.2 }}>
          <View style={styles.inputWrapper}>
            <Text style={styles.label}>e-mail</Text>
            <TextField
              Txt="Email"
              placeholder="example@gmail.com"
              onChangeText={(val) => setEmail(val)}
            />
          </View>
          <View style={styles.inputWrapper}>
            <Text style={styles.label}>password</Text>
            <TextField
              Txt="Password"
              placeholder="*********"
              secureText
              onChangeText={(val) => setPassword(val)}
            />
          </View>
          <View style={styles.inputWrapper}>
            <Text style={styles.label}>validate password</Text>
            <TextField
              Txt="Validate Password"
              placeholder="*********"
              secureText
              onChangeText={(val) => setConfirmPassword(val)}
            />
          </View>
          <View style={styles.BotomTxtWrapper}>
            <Text style={{ fontSize: rf(14), color: "#C90002" }}>
              Do you want to become a cooker and make money?
            </Text>
          </View>
        </View>
        <View style={styles.BtnWrapper}>
          <Button BtnTxt="Next" onPress={onNext} />
        </View>
        <View style={styles.GmailWrapper}>
          <Text style={{ fontSize: rf(14), fontWeight: "bold" }}>OR</Text>
          <TouchableOpacity style={styles.GmailBody}>
            <Image
              style={{ height: "100%", width: "100%" }}
              source={require("../../assets/Gmail.png")}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  BtnWrapper: {
    height: hp("10%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "flex-end",
    alignItems: "center",
  },
  GmailWrapper: {
    height: hp("20%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  GmailBody: {
    height: hp("10"),
    width: hp("10%"),
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 100,
  },
  inputWrapper: {
    width: wp("100%"),
    marginVertical: hp("0.3%"),
  },
  label: {
    fontSize: rf(14),
    fontWeight: "bold",
    paddingHorizontal: wp("5%"),
  },
  BotomTxtWrapper: {
    width: wp("100%"),
    paddingHorizontal: wp("3%"),
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_oauth_token: state.main.get_oauth_token,
});
export default connect(mapStateToProps, { getOAuthToken })(Register1);
