import React, { useEffect, useState } from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { ActivityIndicator, StyleSheet, View } from "react-native";
import Header from "../../Components/Header";
import TextField from "../../Components/TextField";
import Button from "../../Components/Button";
import { connect } from "react-redux";
import { getUserOAuthToken } from "../../state-management/actions/auth/authActions";
import { getUser } from "../../state-management/actions/features/Actions";

const SignIn = (props) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [token, setToken] = useState(null);
  const [loading, setLoading] = useState(false);

  // useEffect(() => {
  //   if (props.get_user_oauth_token?.access_token != null) {
  //     // props.navigation.reset({
  //     //   index: 0,
  //     //   routes: [{ name: "MealDetail" }],
  //     // });
  //     // setLoading(true);
  //     // let data = {
  //     //   username: username,
  //     // };
  //     // props.getUser(props.get_user_oauth_token?.access_token, data , setLoading);
  //   } else {
  //     // console.log(props.get_user_oauth_token.access_token);
  //   }
  // }, [props]);


  useEffect(()=>{
   if(props.get_user_details!=null){
     console.log(props.get_user_details)
   }
  },[props.get_user_details])

  const onLogin = () => {
    setLoading(true);
    let data = {
      username: username,
      password: password,
    };
    if (username != "" && password != "") {
      props.getUserOAuthToken(data, setLoading);
    } else {
      setLoading(false);
      alert("Enter email and password");
      console.log(username, password);
    }
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="black" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Header Title="SignIn" OnBackPress={() => props.navigation.goBack()} />
      <View style={{ marginTop: 20 }}>
        <TextField
          placeholder="Enter username"
          onChangeText={(val) => setUsername(val)}
        />
        <TextField
          placeholder="* * * * * *"
          secureText={true}
          onChangeText={(val) => setPassword(val)}
        />
      </View>
      <View style={styles.BtnContainer}>
        <Button BtnTxt="Log In" onPress={onLogin} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  BtnContainer: {
    paddingHorizontal: wp("5%"),
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_oauth_token: state.main.get_user_oauth_token,
  get_user_details:state.main.get_user_details
});
export default connect(mapStateToProps, { getUserOAuthToken,getUser })(SignIn);
