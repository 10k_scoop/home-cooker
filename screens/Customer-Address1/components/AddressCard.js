import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { FontAwesome5, Entypo } from "@expo/vector-icons";

export default function CustomerCard({ navigation }) {
  return (
    <View style={styles.Discription}>
      <View style={styles.Detail}>
        <Text style={styles.DetailText}>
          11 RUE D’ALGER{"\n"} APPARTEMENTT 42 {"\n"}LILLE 59000{"\n"}{" "}
          06-17-65-66-66
        </Text>
      </View>
      <View style={styles.Icons}>
        <TouchableOpacity style={styles.Circle}>
          <FontAwesome5 name="pen-alt" size={20} color="#fff" />
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.Circle, { backgroundColor: "#FF1E00" }]}
        >
          <Entypo name="cross" size={32} color="#fff" />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  Discription: {
    width: wp("90%"),
    height: hp("20%"),
    borderBottomWidth: 2,
    borderColor: "#e5e5e5",
    flexDirection: "row",
  },
  Detail: {
    width: "60%",
    height: "100%",
    justifyContent: "center",
  },
  DetailText: {
    fontSize: rf(16),
    fontWeight: "700",
    color: "#5E0D13",
    textAlign: "center",
  },
  Circle: {
    width: wp("13%"),
    height: wp("13%"),
    borderRadius: 100,
    backgroundColor: "grey",
    alignItems: "center",
    justifyContent: "center",
  },
  Icons: {
    width: "40%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
  },
});
