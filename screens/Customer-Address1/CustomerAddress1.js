import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import BottomMenu from "../../Components/BottomMenu";
import CustomerCard from "./components/AddressCard";

export default function CustomerAddress1({ navigation }) {
  return (
    <View style={styles.Container}>
      <View style={styles.title}>
        <Text style={styles.titleText}>Mes adresses</Text>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => navigation.navigate("CustomerAddress2")}
        >
          <Text style={styles.BtnText}>AJOUTER</Text>
        </TouchableOpacity>
      </View>
      <CustomerCard />
      <CustomerCard />

      <BottomMenu />
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  title: {
    width: wp("90%"),
    height: hp("15%"),
    alignItems: "flex-end",
    paddingVertical: hp("2%"),
    flexDirection: "row",
    justifyContent: "space-between",
  },
  titleText: {
    fontSize: rf(26),
    fontWeight: "400",
    color: "#5E0D13",
  },
  btn: {
    width: "18%",
    height: "35%",
    borderWidth: 1.5,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#FF1E00",
    borderColor: "#5E0D13",
  },
  BtnText: {
    fontSize: rf(12),
    fontWeight: "700",
    color: "#fff",
  },
});
