import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import BottomMenu from "../../Components/BottomMenu";

export default function CustomerPaiement({ navigation }) {
  return (
    <View style={styles.Container}>
      <View style={styles.title}>
        <Text style={styles.titleText}>Mes moyens de paiement</Text>
      </View>
      {/* Cards */}
      <View style={styles.CardsWrapper}>
        <View style={styles.Img}>
          <Image
            source={require("../../assets/Visa.jpeg")}
            style={{ width: "100%", height: "100%" }}
            resizeMode="cover"
          />
        </View>

        <View style={styles.Detail}>
          <Text style={styles.Font1}>Visa</Text>
          <Text style={styles.Font2}>Se terminant par 4893</Text>
        </View>
      </View>
      <View style={styles.CardsWrapper}>
        <View style={styles.Img}>
          <Image
            source={require("../../assets/Master.jpeg")}
            style={{ width: "100%", height: "100%" }}
            resizeMode="cover"
          />
        </View>

        <View style={styles.Detail}>
          <Text style={styles.Font1}>Mastercard</Text>
          <Text style={styles.Font2}>Se terminant par 8773</Text>
        </View>
      </View>
      {/* Cards */}
      <View style={styles.BtnWrapper}>
        <TouchableOpacity
          style={styles.Btn}
          onPress={() => navigation.navigate("CustomerCB")}
        >
          <Text style={styles.BtnText}>AJOUTER</Text>
        </TouchableOpacity>
      </View>
      <BottomMenu />
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  title: {
    width: wp("90%"),
    height: hp("15%"),
    justifyContent: "flex-end",
    paddingVertical: hp("2%"),
  },
  titleText: {
    fontSize: rf(26),
    fontWeight: "400",
    color: "#5E0D13",
  },
  CardsWrapper: {
    width: wp("90%"),
    height: hp("8%"),
    borderBottomWidth: 1.5,
    borderColor: "#e5e5e5",
    flexDirection: "row",
    alignItems: "center",
  },

  BtnWrapper: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: hp("12%"),
  },
  Btn: {
    width: wp("18%"),
    height: hp("5%"),
    borderWidth: 1.5,
    borderRadius: 6,
    borderColor: "#5E0D13",
    backgroundColor: "#FF1E00",
    alignItems: "center",
    justifyContent: "center",
  },
  BtnText: {
    fontSize: rf(12),
    fontWeight: "700",
    color: "#fff",
  },
  Img: {
    width: "12%",
    height: "45%",
    overflow: "hidden",
  },
  Detail: {
    flex: 1,
    paddingHorizontal: "3%",
  },
  Font1: {
    fontSize: rf(12),
    color: "#5E0D13",
  },
  Font2: {
    fontSize: rf(10),
    color: "grey",
  },
});
