import React, { useEffect, useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  BackHandler,
  Alert,
  ActivityIndicator,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { MaterialIcons, Ionicons } from "@expo/vector-icons";
import MealListCard from "../Components/MealListCard";
import Header1 from "../Components/Header1";
import BottomMenu from "../Components/BottomMenu";
import {
  getUserOAuthToken,
  logout,
} from "../state-management/actions/auth/authActions";
import { getMealsByLocation } from "../state-management/actions/features/Actions";
import { connect } from "react-redux";
import * as Location from "expo-location";
import { AsyncStorage } from "react-native";
const MealList = (props) => {
  const [loading, setLoading] = useState(true);
  const [meals, setMeals] = useState([]);
  const [location, setLocation] = useState(null);
  const [locationName, setLocationName] = useState(null);
  const [userType, setUserType] = useState();

  useEffect(() => {
    (async () => {
      const value = await AsyncStorage.getItem("userSessionToken");
      if (value !== null) {
        props.getMealsByLocation(value, [], setLoading);
        console.log(value);
      } else {
        props.getMealsByLocation(
          props.get_user_oauth_token?.access_token,
          [],
          setLoading
        );
      }
    })().catch((e) => {});
  }, []);

  useEffect(() => {
    if (props.get_meals_by_location != "") {
      setMeals(JSON.parse(props.get_meals_by_location));
    } else {
      //setMeals({content:[]})
    }
  }, [props]);
  //Revoke user from going to authentication screens after login

  useEffect(() => {
    setUserType(JSON.parse(props.get_user_details)?.profil);
  }, [props.get_user_details]);

  //Fetch current location of user
  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        alert("Permission to access location was denied");
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      let locationCoords = {
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
      };
      let locationName = await Location.reverseGeocodeAsync(locationCoords);
      setLocation(location);
      setLocationName(locationName);
    })();
  }, [props]);

  //Fetch current location of user

  if (loading || location == null) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator color="black" size="large" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {/* Header */}
        <Header1
          type="customer"
          locationName={locationName != null ? locationName : []}
          onCartPress={() => alert()}
        />
        {/* Header */}

        {/* Cards */}
        {meals != null && (
          <View style={styles.Cards}>
            {meals?.content?.map((item, index) => {
              let d1 = new Date(item?.useByDate);
              let d2 = new Date();
              let hours = Math.abs(d1 - d2) / 36e5;
              let mealRemainingTime = Math.floor(
                (hours / item?.meal?.timeForConsumption) * 100
              );
              return (
                <MealListCard
                  data={item}
                  hours={Math.floor(hours)}
                  MRT={mealRemainingTime}
                  location={location}
                  key={index}
                  onpress={() =>
                    props.navigation.navigate("MealDetail", {
                      data: item,
                      allMeals: meals?.content,
                      location: location,
                      type: "customer",
                      locationName: locationName,
                    })
                  }
                />
              );
            })}
          </View>
        )}
        {/* Cards */}
      </ScrollView>
      <BottomMenu navigation={props.navigation} userProfile={userType} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Cards: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "25%",
    flexDirection: "column-reverse",
  },
  BottomMenu: {
    width: wp("100%"),
    height: Platform.OS == "ios" ? hp("8%") : hp("7%"),
    backgroundColor: "#222",
    position: "absolute",
    bottom: 0,
    borderTopLeftRadius: 7,
    borderTopRightRadius: 7,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingBottom: Platform.OS == "ios" ? 15 : 5,
  },
  IconCircle: {
    width: hp("5%"),
    height: hp("5%"),
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#fff",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_oauth_token: state.main.get_user_oauth_token,
  get_meals_by_location: state.main.get_meals_by_location,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, {
  getUserOAuthToken,
  logout,
  getMealsByLocation,
})(MealList);
