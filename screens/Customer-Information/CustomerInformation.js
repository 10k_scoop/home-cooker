import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  TextInput,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import BottomMenu from "../../Components/BottomMenu";

export default function CustomerInformation({ navigation }) {
  return (
    <View style={styles.Container}>
      <View style={styles.title}>
        <Text style={styles.titleText}>Mes Informations</Text>
      </View>
      <View style={styles.FieldWrapper}>
        <Text style={styles.Font}>E-mail</Text>
        <TextInput style={styles.TextField} placeholder="1234 5678 9012 3456" />
      </View>
      <View style={styles.FieldWrapper}>
        <Text style={styles.Font}>Username</Text>
        <TextInput style={styles.TextField} placeholder="1234 5678 9012 3456" />
      </View>
      <View style={styles.BtnWrapper}>
        <TouchableOpacity style={styles.Btn}>
          <Text style={styles.BtnText}>Enregistrer</Text>
        </TouchableOpacity>
      </View>
      <BottomMenu />
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  title: {
    width: wp("90%"),
    height: hp("15%"),
    justifyContent: "flex-end",
    paddingVertical: hp("2%"),
    marginBottom: "4%",
  },
  titleText: {
    fontSize: rf(26),
    fontWeight: "400",
    color: "#5E0D13",
  },
  FieldWrapper: {
    width: wp("90%"),
    height: hp("10%"),
    borderBottomWidth: 1.5,
    borderColor: "#e5e5e5",
    justifyContent: "space-evenly",
    marginBottom: "5%",
  },
  TextField: {
    width: "100%",
    height: "50%",
    fontSize: rf(16),
  },
  Font: {
    fontSize: rf(14),
    fontWeight: "400",
    color: "#5E0D13",
  },
  BtnWrapper: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: hp("12%"),
  },
  Btn: {
    width: wp("28%"),
    height: hp("5%"),
    borderWidth: 1.5,
    borderRadius: 6,
    borderColor: "#5E0D13",
    backgroundColor: "#FF1E00",
    alignItems: "center",
    justifyContent: "center",
  },
  BtnText: {
    fontSize: rf(16),
    fontWeight: "700",
    color: "#fff",
  },
});
