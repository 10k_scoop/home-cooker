import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Image,
} from "react-native";
import CartHeader from "./Component/CartHeader";
import CartBtn from "./Component/CartBtn";
export default function CustomerCBPayment({ navigation }) {
  return (
    <View style={styles.container}>
      <CartHeader
        Title="Carte bancaire"
        OnBackPress={() => navigation.goBack()}
      />
      <View style={styles.CardNoWrapper}>
        <View style={styles.CardBody}>
          <Text
            style={{ fontSize: rf(13), fontWeight: "400", color: "#C22B2B" }}
          >
            {" "}
            Numéro de la carte
          </Text>
        </View>
        <View style={styles.CardNoBody}>
          <TextInput
            placeholder="1233 1124 2323 4567"
            style={{ fontSize: rf(13), width: "50%", height: "100%" }}
          />
          <View style={styles.CardOptions}>
            <TouchableOpacity style={{ height: "100%", width: "25%" }}>
              <Image
                style={{ height: "100%", width: "100%" }}
                source={require("../../assets/Visa.jpeg")}
                resizeMode="contain"
              />
            </TouchableOpacity>
            <TouchableOpacity style={{ height: "100%", width: "25%" }}>
              <Image
                style={{ height: "100%", width: "100%" }}
                source={require("../../assets/Master.jpeg")}
                resizeMode="contain"
              />
            </TouchableOpacity>
            <TouchableOpacity style={{ height: "100%", width: "25%" }}>
              <Image
                style={{ height: "100%", width: "100%" }}
                source={require("../../assets/Visa.jpeg")}
                resizeMode="contain"
              />
            </TouchableOpacity>
            <TouchableOpacity style={{ height: "100%", width: "25%" }}>
              <Image
                style={{ height: "100%", width: "100%" }}
                source={require("../../assets/Master.jpeg")}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{ borderBottomWidth: 1, height: 2, borderColor: "#ACADAC" }}
        ></View>
        <View style={styles.DateChifferWrapper}>
          <View style={styles.DateBody}>
            <Text
              style={{ fontSize: rf(12), fontWeight: "400", color: "#C22B2B" }}
            >
              Date d’expiration
            </Text>
            <Text
              style={{ fontSize: rf(12), fontWeight: "400", color: "#ACADAC" }}
            >
              MM/AA
            </Text>
          </View>
          <View style={{ height: "53%", width: "40%", marginTop: "5%" }}>
            <Text
              style={{ fontSize: rf(12), fontWeight: "400", color: "#C22B2B" }}
            >
              CVC / CVV
            </Text>
            <View style={styles.CrifferBBody}>
              <Text
                style={{
                  fontSize: rf(12),
                  fontWeight: "400",
                  color: "#ACADAC",
                }}
              >
                3 chiffres
              </Text>
              <TouchableOpacity style={{ height: "100%", width: "20%" }}>
                <Image
                  style={{ height: "100%", width: "100%" }}
                  source={require("../../assets/Master.jpeg")}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.BtnWrapper}>
        <CartBtn BtnTxt="Payer 2E" />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  CardNoWrapper: {
    height: hp("30%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
  },
  CardBody: {
    marginTop: hp("2%"),
  },
  CardNoBody: {
    height: "25%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  CardOptions: {
    height: "40%",
    width: "45%",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  DateBody: {
    height: "52%",
    width: "40%",
    borderBottomWidth: 1,
    borderColor: "#ACADAC",
    justifyContent: "space-between",
    marginTop: "5%",
    paddingBottom: 7,
  },
  DateChifferWrapper: {
    flexDirection: "row",
    flex: 1,
    justifyContent: "space-between",
  },
  CrifferBBody: {
    flex: 1,
    borderBottomWidth: 1,
    borderColor: "#ACADAC",
    marginTop: "5%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  BtnWrapper: {
    height: hp("12%"),
    width: wp("100%"),
    alignItems: "center",
    justifyContent: "center",
  },
});
