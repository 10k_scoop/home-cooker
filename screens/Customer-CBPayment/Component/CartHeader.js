import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf, RFValue } from "react-native-responsive-fontsize";
import {
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { AntDesign } from "@expo/vector-icons";

export default function CartHeader({ navigation, OnBackPress, Title }) {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={OnBackPress} style={styles.OnBackPress}>
        <AntDesign name="back" size={rf(17)} color="black" />
      </TouchableOpacity>
      <View style={{ alignItems: "center", marginBottom: "2%" }}>
        <Text style={{ fontSize: rf(22), color: "#222" }}>{Title}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: hp("15%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "flex-end",
    borderBottomWidth: 1,
    borderColor: "#ACADAC",
  },
});
