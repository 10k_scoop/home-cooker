import React from "react";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
export default function CartBtn({ BtnTxt, click }) {
    return (
        <TouchableOpacity onPress={click} style={styles.container}>
            <Text style={{ fontSize: rf(20), color: "#fff", fontWeight: "500" }}>{BtnTxt}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        height: hp("6%"),
        width: wp("90%"),
        justifyContent: "center",
        backgroundColor: "#DCA007",
        borderRadius: 100,
        alignItems: "center",
    },
});
