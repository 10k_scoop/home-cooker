import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";

import BottomMenu from "../../Components/BottomMenu";

export default function APropos({ navigation }) {
  return (
    <View style={styles.Container}>
      <View style={styles.title}>
        <Text style={styles.titleText}>À propos</Text>
      </View>
      <View style={styles.Logo}>
        <Image
          source={require(".././../assets/LogoPic.png")}
          style={{ width: "100%", height: "80%" }}
          resizeMode="contain"
        />
        <Text style={styles.logoText}>Version 1.0.0</Text>
      </View>
      <View style={styles.discription}>
        <Text style={styles.font1}>2021 Allcooks.</Text>
        <Text style={styles.font2}>Développé à Lille.</Text>
      </View>

      <BottomMenu />
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  title: {
    width: wp("100%"),
    height: hp("15%"),
    justifyContent: "flex-end",
    paddingHorizontal: wp("4%"),
    paddingVertical: hp("4%"),
  },
  titleText: {
    fontSize: rf(22),
    fontWeight: "400",
    color: "#5E0D13",
  },
  Logo: {
    width: wp("100%"),
    height: hp("40%"),
    alignItems: "center",
  },
  logoText: {
    color: "red",
  },
  discription: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  font1: {
    fontSize: rf(26),
    color: "#FF1E00",
  },
  font2: {
    fontSize: rf(12),
    color: "#FF1E00",
  },
});
