import React, { useState } from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import Header from "../../Components/Header";
import TextField from "../../Components/TextField";
import Button from "../../Components/Button";
import { Picker } from "@react-native-picker/picker";

export default function RegisterBasicInfo({ navigation, route }) {
  const [username, setUsername] = useState("");
  const [age, setAge] = useState("");
  const [sex, setSex] = useState("MALE");

  const data = route.params;

  const onNext = () => {
    if (username != "" && age != "") {
      navigation.navigate("RegisterLocation", {
        email: data.email,
        password: data.password,
        username: username,
        age: age,
        sex: sex,
        oAuthToken: data.oAuthToken,
      });
    } else {
      alert("fill all details");
    }
  };

  return (
    <View style={styles.container}>
      <Header Title="Information" OnBackPress={() => navigation.goBack()} />
      <ScrollView>
        <View style={{ height: hp("9%") * 5.4 }}>
          <View style={styles.inputWrapper}>
            <Text style={styles.label}>Username</Text>
            <TextField
              Txt="Username"
              placeholder="Enter username"
              onChangeText={(val) => setUsername(val)}
            />
          </View>
          <View style={styles.inputWrapper}>
            <Text style={styles.label}>Age</Text>
            <TextField
              Txt="Age"
              placeholder="24"
              onChangeText={(val) => setAge(val)}
              keyboardType="number-pad"
            />
          </View>
          <View style={styles.inputWrapper}>
            <Text style={styles.label}>Sex</Text>
            <View style={styles.picker}>
              <Picker
                selectedValue={sex}
                onValueChange={(itemValue, itemIndex) => setSex(itemValue)}
              >
                <Picker.Item label="Male" value="MALE" />
                <Picker.Item label="Female" value="FEMALE" />
              </Picker>
            </View>
          </View>
        </View>
        <View style={styles.BtnWrapper}>
          <Button BtnTxt="Next" onPress={onNext} />
        </View>
        <View style={styles.BotomTxtWrapper}>
          <Text style={{ fontSize: rf(14), color: "#C90002" }}>
            Do you want to become a cooker and make money?
          </Text>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  BtnWrapper: {
    height: hp("10%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "flex-end",
    alignItems: "center",
  },
  inputWrapper: {
    width: wp("100%"),
    marginVertical: hp("0.3%"),
  },
  label: {
    fontSize: rf(14),
    fontWeight: "bold",
    paddingHorizontal: wp("5%"),
  },
  BotomTxtWrapper: {
    width: wp("100%"),
    paddingHorizontal: wp("3%"),
    justifyContent: "center",
    alignItems: "center",
    marginTop: "6%",
  },
  picker: {
    borderWidth: 1,
    paddingHorizontal: "5%",
    justifyContent: "center",
    width: wp("90%"),
    height: hp("7%"),
    marginHorizontal: wp("5%"),
    margin: 5,
    overflow: "hidden",
  },
});
