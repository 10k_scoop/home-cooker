import React, { useState } from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import Header from "../../Components/Header";
import TextField from "../../Components/TextField";
import Button from "../../Components/Button";
import { Picker } from "@react-native-picker/picker";
import countryCodes from "./countryCodes";
export default function RegisterLocation({ navigation, route }) {
  const [country, setCountry] = useState("");
  const [number, setNumber] = useState("");
  const [street, setStreet] = useState("");
  const [zipCode, setZipCode] = useState("");

  const data = route.params;

  const onNext = () => {
    if (country != "" && number != "") {
      navigation.navigate("RegisterProfile", {
        email: data.email,
        password: data.password,
        username: data.username,
        age: data.age,
        sex: data.sex,
        country: country,
        number: number,
        street: street,
        zipCode: zipCode,
        oAuthToken: data.oAuthToken,
      });
    } else {
      alert("fill all details");
    }
  };

  return (
    <View style={styles.container}>
      <Header Title="Location" OnBackPress={() => navigation.goBack()} />
      <ScrollView style={{ marginTop: 15 }}>
        <View style={styles.inputWrapper}>
          <Text style={styles.label}>Country</Text>
          <View style={styles.picker}>
            <Picker
              selectedValue={country}
              onValueChange={(itemValue, itemIndex) => setCountry(itemValue)}
            >
              {countryCodes.map((item, index) => (
                <Picker.Item key={index} label={item.name} value={item.code} />
              ))}
            </Picker>
          </View>
        </View>
        <View style={styles.inputWrapper}>
          <Text style={styles.label}>Number</Text>
          <TextField
            keyboardType="number-pad"
            Txt="Number"
            placeholder=""
            onChangeText={(val) => setNumber(val)}
          />
        </View>
        <View style={styles.inputWrapper}>
          <Text style={styles.label}>Street</Text>
          <TextField
            Txt="Street"
            placeholder=""
            onChangeText={(val) => setStreet(val)}
          />
        </View>
        <View style={styles.inputWrapper}>
          <Text style={styles.label}>ZipCode</Text>
          <TextField
            keyboardType="number-pad"
            Txt="ZipCode"
            placeholder=""
            onChangeText={(val) => setZipCode(val)}
          />
        </View>

        <View style={styles.BtnWrapper}>
          <Button BtnTxt="Next" onPress={onNext} />
        </View>
        <View style={styles.BotomTxtWrapper}>
          <Text style={{ fontSize: rf(14), color: "#C90002" }}>
            Do you want to become a cooker and make money?
          </Text>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  BtnWrapper: {
    height: hp("15%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
    alignItems: "center",
  },
  inputWrapper: {
    width: wp("100%"),
    marginVertical: hp("0.3%"),
  },
  label: {
    fontSize: rf(14),
    fontWeight: "bold",
    paddingHorizontal: wp("5%"),
  },
  picker: {
    borderWidth: 1,
    paddingHorizontal: "5%",
    justifyContent: "center",
    width: wp("90%"),
    height: hp("7%"),
    marginHorizontal: wp("5%"),
    margin: 5,
    overflow: "hidden",
  },
  BotomTxtWrapper: {
    width: wp("100%"),
    paddingHorizontal: wp("3%"),
    justifyContent: "center",
    alignItems: "center",
  },
});
