import React, { useState } from 'react';
import { ScrollView, StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from 'react-native-responsive-screen';
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from '@expo/vector-icons';

export default function RevenierBtn(props) {

    return (
        <TouchableOpacity style={styles.Card} onPress={props.onClick}>
            <Text style={{ fontSize: rf(18), color: "#fff",fontWeight:"700" }}>Revenir à l’accueil</Text>
        </TouchableOpacity>


    );
}

const styles = StyleSheet.create({
    Card: {
        width: wp('70%'),
        height: hp('6%'),
        borderRadius: 100,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#56534B"

    },


});
