import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  Image,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { EvilIcons, MaterialIcons, Ionicons } from "@expo/vector-icons";
import RevenierBtn from "../Customer-Cart/components/RevenierBtn";

export default function CustomerEmptyCart({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.SearchBar}>
        <View style={styles.InnerSearchBar}>
          <EvilIcons name="search" size={rf(25)} color="grey" />
          <TextInput
            style={styles.TextField}
            placeholder="Rechercher un autre produit"
          />
        </View>
      </View>
      <View style={styles.heading}>
        <Text style={{ fontSize: rf(24), fontWeight: "700" }}>Mon Panier</Text>
      </View>
      <View style={styles.Detail}>
        <Text style={{ fontSize: rf(24), color: "#5E0D13" }}>
          Votre panier est vide
        </Text>
      </View>
      <RevenierBtn />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  SearchBar: {
    width: wp("90%"),
    height: hp("12%"),
    justifyContent: "flex-end",
  },
  InnerSearchBar: {
    width: "100%",
    height: "50%",
    borderWidth: 0.5,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 100,
    paddingHorizontal: 5,
  },
  TextField: {
    width: "90%",
    height: "100%",
    fontSize: rf(18),
    paddingHorizontal: 5,
  },
  heading: {
    width: wp("90%"),
    height: hp("12%"),
    justifyContent: "center",
    alignItems: "center",
  },
  Detail: {
    width: wp("90%"),
    height: hp("25%"),
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "7%",
  },
});
