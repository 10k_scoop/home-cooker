import React, { useState, useEffect } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ImageBackground,
  Image,
  ActivityIndicator,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import {
  AntDesign,
  MaterialCommunityIcons,
  MaterialIcons,
  Ionicons,
  Fontisto,
} from "@expo/vector-icons";
import Header1 from "../../Components/Header1";
import MenuPicCard from "./components/MenuPicCard";
import BottomMenu from "../../Components/BottomMenu";
import CookerCard from "./components/CookerCard";
import {
  getUserOAuthToken,
  logout,
  getOAuthToken,
} from "../../state-management/actions/auth/authActions";
import { connect } from "react-redux";
import {
  getMealsByCooker,
  acceptOrder,
  cancelOrder,
} from "../../state-management/actions/features/Actions";

const CookerMealOrder = (props) => {
  const [loading, setLoading] = useState(true);
  const [refresh, setRefresh] = useState(false);
  const [orders, setOrders] = useState(0);
  const [updateOrder, setUpdateOrder] = useState(false);
  const [meals, setMeals] = useState([]);
  const data = props.route.params;
  const baseUrl = "https://allcooks-backend.herokuapp.com";

  const onRefresh = () => {
    setLoading(true);
    props.getMealsByCooker(
      props.get_user_oauth_token?.access_token,
      setLoading,
      setMeals
    );
  };

  useEffect(() => {
    props.getMealsByCooker(
      props.get_user_oauth_token?.access_token,
      setLoading,
      setMeals
    );
  }, []);

  useEffect(() => {
    if (props.get_meals_by_cooker != "") {
      setMeals(JSON.parse(props.get_meals_by_cooker));
      setOrders(JSON.parse(props.get_meals_by_cooker)?.content);
    } else {
      //setMeals({content:[]})
    }
  }, [props, setRefresh]);

  const onAcceptOrder = async (ref, item) => {
    setUpdateOrder(true);
    setLoading(true);
    console.log(ref);
    await props.acceptOrder(
      props.get_user_oauth_token?.access_token,
      ref,
      setLoading
    );
    onRefresh();
  };
  const onCancelOrder = async (ref, item) => {
    setLoading(true);
    await props.cancelOrder(
      props.get_user_oauth_token?.access_token,
      ref,
      setLoading
    );
    onRefresh();
  };

  return (
    <View style={styles.container}>
      {/* Header */}
      <Header1
        orders={data?.ordersCount}
        locationName={data?.location}
        refresh={true}
        path="cooker"
        onRefreshPress={onRefresh}
      />
      <ScrollView>
        {/* Header */}
        {loading ? (
          <View style={{ alignItems: "center", marginVertical: 20 }}>
            <Text style={{ fontSize: rf(15), fontWeight: "bold" }}>
              Wait fetching orders...
            </Text>
            <ActivityIndicator color="black" size="small" />
          </View>
        ) : (
          <>
            {orders?.length >= 1 && (
              <View style={styles.Cards}>
                {orders?.map((item, index) => {
                  var mealRef = item.reference;
                  var orderDetail = item;
                  return (
                    <View key={index}>
                      {meals?.content.map((item, index) => {
                        if (mealRef == item?.reference) {
                          if (orderDetail?.orders[0]?.stateOrder == "CREATED") {
                            return (
                              <CookerCard
                                onAccept={(ref) => onAcceptOrder(ref, item)}
                                onCancel={(ref) => onCancelOrder(ref, item)}
                                key={index}
                                orderStatus={orderDetail?.orders[0]?.stateOrder}
                                mealDetail={item}
                                orderDetail={orderDetail}
                              />
                            );
                          }
                        }
                      })}
                    </View>
                  );
                })}
              </View>
            )}
          </>
        )}
      </ScrollView>
      <BottomMenu navigation={props?.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },

  Discription3: {
    width: wp("100%"),
    height: hp("2%"),
    justifyContent: "center",
    paddingHorizontal: 10,
    marginTop: 20,
  },
  MenuCards: {
    width: wp("100%"),
    height: hp("17%"),
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 5,
  },
  MenuCardsWrapper: {
    position: "absolute",
    bottom: hp("8%"),
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_oauth_token: state.main.get_user_oauth_token,
  get_user_details: state.main.get_user_details,
  get_meals_by_cooker: state.main.get_meals_by_cooker,
});
export default connect(mapStateToProps, {
  getUserOAuthToken,
  logout,
  getMealsByCooker,
  acceptOrder,
  cancelOrder,
})(CookerMealOrder);
