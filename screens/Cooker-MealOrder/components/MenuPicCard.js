import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ImageBackground,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";

export default function MenuPicCard({ navigation }) {
  return (
    <TouchableOpacity style={styles.Img}>
      <ImageBackground
        style={{ width: "100%", height: "100%" }}
        source={require("../../../assets/MealPic.jpg")}
        resizeMode="cover"
      ></ImageBackground>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  Img: {
    width: wp("27%"),
    height: hp("14%"),
    borderRadius: 10,
    overflow: "hidden",
    marginRight: 4,
    marginLeft: 4,
  },
});
