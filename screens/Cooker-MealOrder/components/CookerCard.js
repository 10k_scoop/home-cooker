import React, { useEffect } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { Entypo } from "@expo/vector-icons";

export default function CookerCard(props) {
  let OD = props?.orderDetail;
  let MD = props?.mealDetail;

  var img =
    "https://images.unsplash.com/photo-1519708227418-c8fd9a32b7a2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bWVhbHxlbnwwfHwwfHw%3D&w=1000&q=80";

  let mealImage = `data:image/png;base64,${MD?.meal?.photos[0]?.image?.data}`;

  return (
    <TouchableOpacity style={styles.Card} onPress={props.onAdd}>
      <View style={styles.firstRow}>
        <Text style={styles.font1}>
          {props.mealDetail?.meal?.title}
          <Text style={styles.font2}> à 1,2 km</Text>
        </Text>
      </View>
      <View style={styles.SecondRow}>
        <View style={styles.Image}>
          <Image
            source={{ uri: mealImage }}
            style={{ width: "100%", height: "100%" }}
            resizeMode="cover"
          />
        </View>
        <View style={styles.Detail}>
          <Text style={styles.DetailFont1}>
            {OD?.orders[0]?.quantity} parts : total{" "}
            <Text style={styles.DetailFont2}>{OD?.orders[0]?.quantity * MD.price}€</Text>
          </Text>
          <View style={styles.InnerDetail}>
            <View style={styles.profile}>
              <Image
                source={require("../../../assets/GirlProfile.jpg")}
                style={{ width: "100%", height: "100%" }}
                resizeMode="cover"
              />
            </View>
            {props?.orderStatus == "VALIDATED" ? (
              <Text style={[styles.DetailFont2,{fontSize:13,color:'green'}]}>Order Accepted</Text>
            ) : (
              <>
                <TouchableOpacity
                  style={styles.circle}
                  onPress={() => props.onCancel(OD?.orders[0]?.reference)}
                >
                  <Entypo name="cross" size={rf(20)} color="#fff" />
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.circle, { backgroundColor: "green" }]}
                  onPress={() => props.onAccept(OD?.orders[0]?.reference)}
                >
                  <Entypo name="check" size={rf(20)} color="#ffff" />
                </TouchableOpacity>
              </>
            )}
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  Card: {
    width: wp("95%"),
    height: hp("20%"),
    borderRadius: 10,
    backgroundColor: "#ECECEC",
    overflow: "hidden",
    marginTop: hp("3%"),
  },
  firstRow: {
    alignItems: "center",
    height: "22%",
  },
  font1: {
    fontSize: rf(18),
    fontWeight: "700",
    color: "#5E0D13",
    textAlign: "left",
    width: "100%",
    left: 10,
  },
  font2: {
    fontSize: rf(12),
    fontWeight: "300",
    color: "#5E0D13",
  },
  SecondRow: {
    flex: 1,
    flexDirection: "row",
  },
  Image: {
    flex: 0.5,
    borderRadius: 10,
    overflow: "hidden",
  },
  Detail: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
  },
  InnerDetail: {
    flexDirection: "row",
    width: "100%",
    height: "50%",
    alignItems: "center",
    justifyContent: "center",
  },
  DetailFont1: {
    fontSize: rf(20),
    fontWeight: "700",
    color: "#5E0D13",
  },
  DetailFont2: {
    fontSize: rf(20),
    fontWeight: "700",
    color: "#FF0013",
  },
  profile: {
    width: wp("12%"),
    height: wp("12%"),
    borderRadius: 100,
    borderWidth: 2,
    borderColor: "#FFEE59",
    overflow: "hidden",
  },
  circle: {
    width: wp("7%"),
    height: wp("7%"),
    borderRadius: 100,
    backgroundColor: "#FF1E00",
    marginLeft: "2%",
    alignItems: "center",
    justifyContent: "center",
  },
});
