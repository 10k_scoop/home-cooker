import React, { useEffect, useState } from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  ActivityIndicator,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import Header from "../../Components/Header";
import TextField from "../../Components/TextField";
import Button from "../../Components/Button";
import { getOAuthToken } from "../../state-management/actions/auth/authActions";
import { connect } from "react-redux";

const CookerRegister = (props) => {
  const [loading, setLoading] = useState(true);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [validatePass, setValidatePass] = useState("");
  const [token, setToken] = useState(null);

  useEffect(() => {
    props.getOAuthToken(setLoading);
  }, []);

  useEffect(() => {
    if (props.get_oauth_token?.access_token) {
      setToken(props.get_oauth_token?.access_token);
    } else {
      console.log(props.errors);
    }
  }, [props]);
  console.log(token);
  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="#222" />
      </View>
    );
  }

  const onNext = () => {
    if (email == "" || password == "" || validatePass == "") {
      alert("Fill all fields");
    } else {
      if (password == validatePass) {
        props.navigation.navigate("CookerInformation", {
          email: email,
          pass: password,
          oAuthToken: token,
        });
      } else {
        alert("Password doesn't match");
      }
    }
  };

  return (
    <View style={styles.container}>
      <Header
        Title="Welcome Cooker"
        OnBackPress={() => props.navigation.goBack()}
      />
      <ScrollView style={{ marginTop: 15 }}>
        <View style={{ height: hp("9%") * 5.2 }}>
          <View style={[styles.inputWrapper, { marginTop: hp("2%") }]}>
            <Text style={styles.label}>Email</Text>
            <TextField
              Txt="Email"
              placeholder="example@gmail.com"
              onChangeText={(val) => setEmail(val)}
            />
          </View>
          <View style={styles.inputWrapper}>
            <Text style={styles.label}>Password</Text>
            <TextField
              Txt="Password"
              placeholder="*********"
              secureText
              onChangeText={(val) => setPassword(val)}
            />
          </View>
          <View style={styles.inputWrapper}>
            <Text style={styles.label}>Confirm password</Text>
            <TextField
              Txt="Validate Password"
              placeholder="*********"
              secureText
              onChangeText={(val) => setValidatePass(val)}
            />
          </View>
        </View>
        <View style={styles.BtnWrapper}>
          <Button BtnTxt="Next" onPress={onNext} />
        </View>
        <View style={styles.GmailWrapper}>
          <Text style={{ fontSize: rf(14), fontWeight: "bold" }}>OR</Text>
          <TouchableOpacity style={styles.GmailBody}>
            <Image
              style={{ height: "100%", width: "100%" }}
              source={require("../../assets/Gmail.png")}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  BtnWrapper: {
    height: hp("10%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "flex-end",
    alignItems: "center",
  },
  GmailWrapper: {
    height: hp("20%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  GmailBody: {
    height: hp("10"),
    width: hp("10%"),
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 100,
  },
  inputWrapper: {
    width: wp("100%"),
    marginVertical: hp("0.3%"),
  },
  label: {
    fontSize: rf(12),
    fontWeight: "500",
    paddingHorizontal: wp("5%"),
    left: 5,
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_oauth_token: state.main.get_oauth_token,
});
export default connect(mapStateToProps, { getOAuthToken })(CookerRegister);
