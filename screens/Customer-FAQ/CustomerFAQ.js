import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import BottomMenu from "../../Components/BottomMenu";
import FAQCards from "./components/FAQCards";

export default function CustomerFAQ({ navigation }) {
  return (
    <View style={styles.Container}>
      <View style={styles.title}>
        <Text style={styles.titleText}>FAQ</Text>
      </View>
      <FAQCards Question title="Question 1" />
      <FAQCards Question title="Question 2" />
      <FAQCards Question title="Question 3" />
      <FAQCards />
      <FAQCards />
      <FAQCards />
      <FAQCards />
      <FAQCards />
      <View style={styles.discription}>
        <Text>Contact support</Text>
      </View>
      <BottomMenu />
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  title: {
    width: wp("100%"),
    height: hp("15%"),
    justifyContent: "flex-end",
    paddingHorizontal: wp("4%"),
    paddingVertical: hp("4%"),
  },
  titleText: {
    fontSize: rf(22),
    fontWeight: "400",
    color: "#5E0D13",
  },
  discription: {
    width: wp("100%"),
    height: hp("10%"),
    alignItems: "center",
    justifyContent: "center",
  },
});
