import React, { useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import {
  MaterialCommunityIcons,
  MaterialIcons,
  Ionicons,
  SimpleLineIcons,
  Zocial,
  Entypo,
} from "@expo/vector-icons";

export default function FAQCards(props) {
  return (
    <TouchableOpacity style={styles.Card} onPress={props.onClick}>
      {props.Question ? (
        <SimpleLineIcons name="question" size={24} color="#fff" />
      ) : null}

      <Text style={styles.font}>{props.title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  Card: {
    width: wp("100%"),
    height: hp("6%"),
    backgroundColor: "#5E0D13",
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: wp("2%"),
    borderBottomWidth: 1,
    borderColor: "#fff",
    marginBottom: "2%",
  },
  font: {
    fontSize: rf(16),
    fontWeight: "400",
    color: "#fff",
    marginLeft: "2%",
  },
});
