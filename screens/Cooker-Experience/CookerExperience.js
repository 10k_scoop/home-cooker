import React, { useState } from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  Image,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from "react-native";
import { Entypo, FontAwesome5 } from "@expo/vector-icons";
import Header from "../../Components/Header";
import Button from "../../Components/Button";
import { Picker } from "@react-native-picker/picker";
export default function CookerExperience(props) {
  const [experience, setExperience] = useState();
  const [desc, setDesc] = useState("");
  const data = props.route.params;

  const onNext = () => {
    if (experience == null) {
      alert("Fill all fields");
    } else {
      props.navigation.navigate("CookerProfile", {
        email: data.email,
        pass: data.pass,
        age: data.age,
        gender: data.gender,
        username: data.username,
        oAuthToken: data.oAuthToken,
        experience: experience,
        phoneNumber:data.phoneNumber,
        desc: desc,
        address: {
          country: data.address.country,
          number: data.address.number,
          street: data.address.street,
          zipCode: data.address.zipCode,
          state: null,
        },
      });
    }
  };

  return (
    <View style={styles.container}>
      <Header
        Title="Your experience"
        OnBackPress={() => props.navigation.goBack()}
      />
      <ScrollView>
        <View style={styles.ExperienceDetail}>
          <Text style={{ fontSize: rf(14), color: "#5E0D13" }}>
            Years of experience in cooking{" "}
          </Text>
        </View>
        <View style={styles.picker}>
          <Picker
            selectedValue={experience}
            onValueChange={(itemValue, itemIndex) => setExperience(itemValue)}
            itemStyle={{ fontSize: rf(13) }}
          >
            <Picker.Item label="Select experience" value={null} />
            <Picker.Item label="1" value={1} />
            <Picker.Item label="2" value={2} />
            <Picker.Item label="3" value={3} />
            <Picker.Item label="4" value={4} />
            <Picker.Item label="5" value={5} />
            <Picker.Item label="6+" value={6} />
          </Picker>
        </View>
        <View style={styles.Describe}>
          <Text style={{ fontSize: rf(16), color: "#5E0D13" }}>
            Describe yourself in 3000 letters maximum{"\n"} (best skills etc..){" "}
          </Text>
        </View>
        <View style={styles.TextField}>
          <TextInput
            style={{
              width: "100%",
              marginHorizontal: 5,
              fontSize: rf(16),
            }}
            onChangeText={(val) => setDesc(val)}
            value={desc}
            multiline={true}
          />
        </View>
        <View style={styles.SocialMedia}>
          <Text style={{ fontSize: rf(16), color: "#5E0D13" }}>
            Connect your social media
          </Text>
        </View>
        <View style={styles.Icons}>
          <View style={styles.InnerIcon}>
            <Entypo name="facebook" size={rf(34)} color="black" />
            <FontAwesome5 name="instagram-square" size={rf(34)} color="black" />
            <View style={styles.Circle}>
              <Entypo name="pinterest" size={rf(25)} color="#fff" />
            </View>
          </View>
        </View>
        <Button BtnTxt="Next" onPress={onNext} />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  ExperienceDetail: {
    width: wp("90%"),
    height: hp("4%"),
    justifyContent: "center",
  },
  Arrow: {
    width: wp("90%"),
    height: hp("9%"),
  },
  ArrowInner: {
    width: "18%",
    height: "90%",
    justifyContent: "center",
    alignItems: "center",
  },
  Describe: {
    width: wp("90%"),
    height: hp("9%"),
    justifyContent: "center",
  },
  TextField: {
    width: wp("90%"),
    height: hp("20%"),
    borderWidth: 1,
    borderColor: "#5E0D13",
    padding: 10,
    borderRadius: 10,
  },
  SocialMedia: {
    width: wp("90%"),
    height: hp("10%"),
    justifyContent: "center",
  },
  Icons: {
    width: wp("90%"),
    height: hp("7%"),
    marginBottom: "12%",
  },
  InnerIcon: {
    width: "45%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
  Circle: {
    width: hp("5%"),
    height: hp("5%"),
    backgroundColor: "#222",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 100,
  },

  label: {
    fontSize: rf(12),
    fontWeight: "500",
    paddingHorizontal: wp("5%"),
    left: 5,
  },
  picker: {
    borderWidth: 1,
    paddingHorizontal: "5%",
    justifyContent: "center",
    height: hp("7%"),
    overflow: "hidden",
    marginTop: 10,
    borderColor: "#e5e5e5",
    borderRadius: 10,
  },
});
