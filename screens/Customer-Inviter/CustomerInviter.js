import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import BottomMenu from "../../Components/BottomMenu";
import { Feather } from "@expo/vector-icons";

export default function CustomerInviter({ navigation }) {
  return (
    <View style={styles.Container}>
      <View style={styles.title}>
        <Text style={styles.titleText}>Inviter des amis</Text>
      </View>
      <View style={styles.logoPic}>
        <Image
          source={require("../../assets/Friends.png")}
          style={{ width: "100%", height: "100%" }}
          resizeMode="contain"
        />
      </View>
      <View style={styles.discription}>
        <Text style={styles.font1}>Faites goûter AllCooks à un ami</Text>
        <Text style={styles.font2}>root.it/jane</Text>
        <TouchableOpacity style={styles.Btn}>
          <Feather name="upload" size={18} color="#fff" />
          <Text style={styles.font3}>Partager</Text>
        </TouchableOpacity>
      </View>

      <BottomMenu />
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  title: {
    width: wp("100%"),
    height: hp("15%"),
    justifyContent: "flex-end",
    paddingHorizontal: wp("4%"),
    paddingVertical: hp("4%"),
  },
  titleText: {
    fontSize: rf(26),
    fontWeight: "400",
    color: "#5E0D13",
  },
  logoPic: {
    width: wp("100%"),
    height: hp("15%"),
  },
  discription: {
    width: wp("100%"),
    height: hp("18%"),
    alignItems: "center",
    justifyContent: "center",
  },
  Btn: {
    width: "35%",
    height: "25%",
    backgroundColor: "#5E0D13",
    borderRadius: 5,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
    paddingHorizontal: "4%",
  },
  font1: {
    fontSize: rf(16),
    color: "#5E0D13",
    fontWeight: "700",
    marginBottom: 5,
  },
  font2: {
    fontSize: rf(14),
    color: "#DCA007",
    marginBottom: 5,
  },
  font3: {
    fontSize: rf(14),
    fontWeight: "700",
    color: "#fff",
  },
});
