import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import DashboardCards from "./components/DashboardCards";
import BottomMenu from "../../Components/BottomMenu";
import { logout } from "../../state-management/actions/auth/authActions";
import { connect } from "react-redux";

const CookerDashboard = (props) => {
  return (
    <View style={styles.Container}>
      <View style={styles.title}>
        <Text style={styles.titleText}>Dashboard</Text>
      </View>
      <DashboardCards title="Mes ventes" Command />
      <View style={styles.EmptyView}></View>
      <DashboardCards title="Mes informations" profile />
      <DashboardCards title="Mes Récompenses" medal />
      <View style={styles.EmptyView}></View>
      <DashboardCards title="Mes adresses" location />
      <DashboardCards title="Mes Messages" Msgs />
      <View style={styles.EmptyView}></View>
      <DashboardCards title="Inviter des amis" Invite />
      <View style={styles.EmptyView}></View>
      <DashboardCards title="FAQ" Question />
      <View style={styles.EmptyView}></View>
      <DashboardCards
        title="Déconnexion"
        upload
        onClick={() => {
          props.logout()
          props.navigation.navigate("ChooseOption")
        }
        }
      />
      <View style={styles.EmptyView}></View>
      <DashboardCards title="À propos" />
      <BottomMenu navigation={props.navigation} userProfile={JSON.parse(props.get_user_details)?.profil} />
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  title: {
    width: wp("100%"),
    height: hp("15%"),
    justifyContent: "flex-end",
    paddingHorizontal: wp("4%"),
    paddingVertical: hp("4%"),
  },
  titleText: {
    fontSize: rf(22),
    fontWeight: "400",
    color: "#5E0D13",
  },
  EmptyView: {
    width: wp("100%"),
    height: hp("4%"),
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details:state.main.get_user_details
});
export default connect(mapStateToProps, { logout })(CookerDashboard);
