import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from "@expo/vector-icons";

export default function Header(propos) {
  return (
    <View style={styles.Container}>
      <View style={styles.Icon}>
        <TouchableOpacity onPress={propos.onBack}>
          <AntDesign name="back" size={24} color="black" />
        </TouchableOpacity>
      </View>
      <View style={styles.title}>
        <Text style={styles.font}>Carte bancaire</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    width: wp("100%"),
    height: hp("13%"),
    borderBottomWidth: 1.5,
    borderColor: "#e5e5e5",
    flexDirection: "row",
  },
  Icon: {
    width: "15%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-end",
    paddingVertical: "4%",
  },
  font: {
    fontSize: rf(24),
    fontWeight: "700",
    marginRight: wp("10%"),
  },
});
