import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  TextInput,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import BottomMenu from "../../Components/BottomMenu";
import Header from "./components/Header";

export default function CustomerCB({ navigation }) {
  return (
    <View style={styles.Container}>
      <Header />
      {/* Credit Cards */}
      <View style={styles.NumberRow}>
        <Text style={styles.Font}>Numéro de la carte</Text>
        <View style={styles.cardsWrapper}>
          <TextInput
            style={styles.NumField}
            placeholder="1234 5678 9012 3456"
          />
          <View style={styles.cardsRow}>
            <TouchableOpacity style={styles.Wrapper}>
              <Image
                source={require("../../assets/Visa.jpeg")}
                style={{ width: "100%", height: "100%" }}
                resizeMode="cover"
              />
            </TouchableOpacity>
            <TouchableOpacity style={styles.Wrapper}>
              <Image
                source={require("../../assets/Master.jpeg")}
                style={{ width: "100%", height: "100%" }}
                resizeMode="cover"
              />
            </TouchableOpacity>
            <TouchableOpacity style={styles.Wrapper}>
              <Image
                source={require("../../assets/Visa.jpeg")}
                style={{ width: "100%", height: "100%" }}
                resizeMode="cover"
              />
            </TouchableOpacity>
            <TouchableOpacity style={styles.Wrapper}>
              <Image
                source={require("../../assets/Master.jpeg")}
                style={{ width: "100%", height: "100%" }}
                resizeMode="cover"
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      {/* Credit Cards */}
      {/* Date and CVC */}
      <View style={styles.DateCVCWrapper}>
        <View style={styles.DateRow}>
          <Text style={styles.Font}>Date d’expiration</Text>
          <TextInput style={styles.DateField} placeholder="MM/AA" />
        </View>
        <View style={styles.DateCVCWrapper}>
          <View style={styles.DateRow}>
            <Text style={styles.Font}>CVC / CVV</Text>
            <TextInput style={styles.DateField} placeholder="3 chiffres" />
          </View>
        </View>
      </View>
      <View style={styles.BtnWrapper}>
        <TouchableOpacity style={styles.Btn}>
          <Text style={styles.BtnText}>AJOUTER</Text>
        </TouchableOpacity>
      </View>
      {/* Date and CVC */}
      <BottomMenu />
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  title: {
    width: wp("90%"),
    height: hp("15%"),
    justifyContent: "flex-end",
    paddingVertical: hp("2%"),
  },
  titleText: {
    fontSize: rf(26),
    fontWeight: "400",
    color: "#5E0D13",
  },
  NumberRow: {
    width: wp("90%"),
    height: hp("10%"),
    borderBottomWidth: 1.5,
    borderColor: "#e5e5e5",
    justifyContent: "space-evenly",
    marginTop: hp("4%"),
  },
  cardsWrapper: {
    width: "100%",
    height: "50%",
    flexDirection: "row",
  },
  NumField: {
    width: "55%",
    height: "100%",
    fontSize: rf(16),
  },
  cardsRow: {
    width: "45%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  Wrapper: {
    width: wp("8%"),
    height: hp("3%"),
    borderRadius: 5,
    overflow: "hidden",
    marginLeft: 2,
  },
  Font: {
    fontSize: rf(14),
    color: "#C22B2B",
  },
  DateCVCWrapper: {
    width: wp("90%"),
    height: hp("10%"),
    flexDirection: "row",
    alignItems: "flex-end",
  },
  DateRow: {
    width: "40%",
    height: "70%",
    borderBottomWidth: 1.5,
    borderColor: "#e5e5e5",
    justifyContent: "space-evenly",
    marginRight: wp("15%"),
  },
  DateField: {
    width: "100%",
    height: "40%",
  },
  BtnWrapper: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: hp("12%"),
  },
  Btn: {
    width: wp("18%"),
    height: hp("5%"),
    borderWidth: 1.5,
    borderRadius: 6,
    borderColor: "#5E0D13",
    backgroundColor: "#FF1E00",
    alignItems: "center",
    justifyContent: "center",
  },
  BtnText: {
    fontSize: rf(12),
    fontWeight: "700",
    color: "#fff",
  },
});
