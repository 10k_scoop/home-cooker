import React, { useState, useEffect } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import BottomMenu from "../../Components/BottomMenu";
import { connect } from "react-redux";
import { logout } from "../../state-management/actions/auth/authActions";
import { CardField, useConfirmPayment } from "@stripe/stripe-react-native";
import { StripeProvider } from "@stripe/stripe-react-native";

const PaymentMethod = (props) => {
  let params = props.route.params;

  const [cardDetails, setCardDetails] = useState();
  const [loader, setLoader] = useState(false);
  const [paymentDone, setPaymentDone] = useState(false);
  const { confirmPayment, loading } = useConfirmPayment();

  return (
    <View style={styles.container}>
      {loader && (
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <ActivityIndicator color="black" size="large" />
        </View>
      )}
      <ScrollView>
        <View style={styles.wrapper}>
          <View style={styles.headerTitleWrapper}>
            <Text style={styles.headerTitle}>Payment Method</Text>
          </View>

          <Text style={styles.title}>Enter Card details</Text>
          <CardField
            postalCodeEnabled={true}
            placeholder={{
              number: "4242 4242 4242 4242",
            }}
            cardStyle={styles.card}
            style={styles.cardContainer}
            onCardChange={(cardDetails) => {
              setCardDetails(cardDetails);
            }}
          />
          <TouchableOpacity
            style={styles.addPaymentMethodButton}
            disabled={loading}
          >
            <Text style={styles.addPaymentMethodButtonText}>Pay now</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
      <BottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  BottomMenu: {
    width: wp("100%"),
    height: Platform.OS == "ios" ? hp("8%") : hp("7%"),
    backgroundColor: "#222",
    position: "absolute",
    bottom: 0,
    borderTopLeftRadius: 7,
    borderTopRightRadius: 7,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingBottom: Platform.OS == "ios" ? 15 : 5,
  },
  wrapper: {
    flex: 1,
    width: "100%",
    paddingTop: hp("5%"),
    paddingHorizontal: wp("5%"),
  },
  addPaymentMethodButton: {
    width: wp("90%"),
    height: hp("5%"),
    backgroundColor: "#F5CA48",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp("3%"),
    borderRadius: 5,
    paddingHorizontal: wp("5%"),
    alignSelf: "center",
  },
  addPaymentMethodButtonText: {
    fontSize: rf(18),
    color: "#fff",
    marginLeft: 10,
  },
  cardContainer: {
    height: 50,
    marginVertical: 10,
  },
  card: {
    backgroundColor: "#efefef",
    borderRadius: 10,
  },
  loader: {
    position: "absolute",
    zIndex: 99999999,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%",
    opacity: 0.5,
  },
  title: {
    fontSize: rf(18),
    marginLeft: 10,
    color: "#A59F9F",
    marginVertical: 5,
  },
  headerTitleWrapper: {
    flex: 0.5,
    marginTop: hp("5%"),
  },
  headerTitle: {
    fontSize: rf(22),
    marginLeft: 10,
    color: "#222",
    fontWeight: "600",
    marginVertical: 5,
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_oauth_token: state.main.get_user_oauth_token,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { logout })(PaymentMethod);
