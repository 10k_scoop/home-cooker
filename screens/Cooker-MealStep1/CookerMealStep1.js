import React, { useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from "@expo/vector-icons";
import BottomMenu from "../../Components/BottomMenu";
import { connect } from "react-redux";
import { getUserOAuthToken } from "../../state-management/actions/auth/authActions";

const CookerMealStep1 = (props) => {
  const [title, setTitle] = useState("");
  const [desc, setDesc] = useState("");

  const onNext = () => {
    if (title != "" && desc != "") {
      let data = {
        title: title,
        desc: desc,
      };
      props.navigation.navigate("CookerMealStep2", { data: data });
    } else {
      alert("Fill all details");
    }
  };

  return (
    <View style={styles.container}>
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
      >
        <ScrollView>
          <View style={styles.innerWrapper}>
            {/* Header */}
            <View style={styles.Header}>
              <TouchableOpacity onPress={() => props.navigation.goBack()}>
                <AntDesign name="back" size={rf(18)} color="black" />
              </TouchableOpacity>
            </View>
            {/* Header */}
            <View style={styles.heading}>
              <Text style={styles.headingText}>Add new meal</Text>
            </View>
            <View style={styles.title}>
              <Text style={styles.titleText}>Title</Text>
              <TextInput
                style={styles.titleField}
                placeholder="Hamburger Maison"
                placeholderTextColor="#5E0D13"
                onChangeText={(val) => setTitle(val)}
              />
            </View>
            <View style={styles.discription}>
              <Text style={styles.discriptionText}>
                Description (ingredients)
              </Text>
              <TextInput
                style={styles.discriptionField}
                placeholder="Hamburger Maisoninde autem eius quem ipse possis non 
          tamen sustinere si eius autem fratrem si ille enim ille neque"
                placeholderTextColor="#5E0D13"
                multiline={true}
                onChangeText={(val) => setDesc(val)}
              />
            </View>
            <View style={styles.BtnWrapper}>
              <TouchableOpacity style={styles.Btn} onPress={() => onNext()}>
                <Text style={styles.BtnText}>Next</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Header: {
    width: wp("100%"),
    height: hp("10%"),
    justifyContent: "flex-end",
    paddingHorizontal: wp("2%"),
  },
  heading: {
    width: wp("90%"),
    height: hp("8%"),
    justifyContent: "center",
  },
  headingText: {
    fontSize: rf(26),
    fontWeight: "400",
    color: "#5E0D13",
  },
  title: {
    width: wp("90%"),
    height: hp("10%"),
    justifyContent: "space-around",
  },
  titleField: {
    width: "100%",
    height: "60%",
    borderWidth: 1.5,
    fontSize: rf(14),
    paddingHorizontal: wp("3%"),
    borderColor: "#5E0D13",
    borderRadius: 10,
  },
  titleText: {
    fontSize: rf(14),
    color: "#5E0D13",
  },
  discription: {
    width: wp("90%"),
    height: hp("30%"),
    justifyContent: "space-around",
  },
  discriptionField: {
    width: "100%",
    height: "80%",
    borderWidth: 1.5,
    fontSize: rf(14),
    paddingHorizontal: wp("3%"),
    borderColor: "#5E0D13",
    borderRadius: 10,
  },
  discriptionText: {
    fontSize: rf(14),
    color: "#5E0D13",
  },
  BtnWrapper: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: hp("10%"),
  },
  Btn: {
    width: wp("90%"),
    height: hp("7%"),
    borderRadius: 10,
    backgroundColor: "#5E0D13",
    alignItems: "center",
    justifyContent: "center",
  },
  BtnText: {
    fontSize: rf(14),
    fontWeight: "700",
    color: "#fff",
  },
  innerWrapper:{
    flex:1,
    alignItems:'center',
    height:hp('100%')
  }
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_oauth_token: state.main.get_user_oauth_token,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { getUserOAuthToken })(CookerMealStep1);
