import React, { useEffect, useState } from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  TouchableOpacity,
} from "react-native";
import Header from "../../Components/Header";
import Button from "./components/Button";
import { connect } from "react-redux";
import { createUser } from "../../state-management/actions/features/Actions";
import { AntDesign } from "@expo/vector-icons";

const CustomerRegulation = (props) => {
  const [isSelected, setIsSelected] = useState(false);
  const [loading, setLoading] = useState(false);
  const [responseCode, setResponseCode] = useState();

  const data = props.route.params;
  let requestData = {
    email: data?.email,
    password: data?.password,
    username: data?.username,
    phoneNumber: data?.phoneNumber,
    profil: "CUSTOMER",
    address: {
      number: data?.address?.number,
      street:  data?.address?.street,
      zipCode:  data?.address?.zipCode,
      country:  data?.address?.country,
      state: null,
    },
    totalRating: { totalComments: 0, totalRatings: 0, avgScore: 0 },
    meals: null,
    favoriteMeals: null,
  };
  const onFinish = () => {
    if (isSelected) {
      setLoading(true);
      props.createUser(data, setLoading, setResponseCode, requestData);
    } else {
      alert("Accept terms & conditions !");
    }
  };

  useEffect(() => {
    if (responseCode == 200 || responseCode == 201) {
      alert("Congrats! account created successfully");
      props.navigation.reset({
        index: 0,
        routes: [{ name: "MealList", type: "customer" }],
      });
    } else if (responseCode == 400 || responseCode == 401) {
      alert("Something went wrong! please check details again ");
    } else {
    }

    if (responseCode == 409) {
      alert("user already exists! ");
    } else {
      console.log(responseCode);
    }
  }, [props]);

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="black" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Header
        Title="Regulation"
        OnBackPress={() => props.navigation.goBack()}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Discription2}>
          <Text
            style={{ fontSize: rf(16), textAlign: "justify", marginTop: 10 }}
          >
            populationum et caedium, milite per omnia diffuso propinqua, magnis
            undique praesidiis conmunitam. Et est admodum mirum videre plebem
            innumeram mentibus ardore quodam infuso cum dimicationum curulium
            eventu pendentem. haec similiaque memorabile nihil vel serium agi
            Romae permittunt. ergo redeundum ad textum. Metuentes igitur idem
            latrones Lycaon Metuentes igitur idem latrones Lycaoniam magna parte
            campestrem cum se inparesnostris fore congressione stataria
            documentis frequentibus scirent, tramitibus deviis petivere
            Pamphyliam diu quidem intactam sed timore populationum et caedium,
            milite per omnia diffuso propinqua, magnis undique praesidiis
            conmunitam. Et est admodum mirum videre plebem innumeram mentibus
            ardore quodam infuso cum dimicationum curulium eventu pendentem.
            haec similiaque memorabile nihil vel serium agi Romae permittunt.
            ergo redeundum ad textum. Metuentes igitur idem latrones Lycaon
          </Text>
        </View>

        <View style={styles.TickAgree}>
          <TouchableOpacity
            style={styles.checkbox}
            onPress={() => setIsSelected(!isSelected)}
          >
            {isSelected && <AntDesign name="check" size={16} color="black" />}
          </TouchableOpacity>
          <Text style={{ marginLeft: 5 }}>
            Yes , Mr {props.route?.params?.username || ""} I’m agree with terms
            and conditions
          </Text>
        </View>
        <View style={{ paddingBottom: hp("1.5%") }}>
          <Button BtnTxt="Finish" onPress={onFinish} />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  BtnWrapper: {
    height: hp("15%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
    alignItems: "center",
  },
  ProfilePicWrapper: {
    height: hp("30%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
    alignItems: "center",
  },
  TextWrapper: {
    height: hp("5%"),
    justifyContent: "center",
    paddingHorizontal: wp("5%"),
  },
  ProfilePicBody: {
    height: hp("25%"),
    width: hp("25%"),
    borderRadius: hp("25%") / 2,
    backgroundColor: "#C4C4C4",
    justifyContent: "center",
    alignItems: "center",
  },
  BotomTxtWrapper: {
    width: wp("100%"),
    paddingHorizontal: wp("3%"),
    justifyContent: "center",
    alignItems: "center",
  },
  Discription1: {
    width: wp("90%"),
    justifyContent: "center",
    alignItems: "center",
  },
  Discription2: {
    width: wp("90%"),
    alignItems: "center",
  },
  Discription3: {
    width: wp("90%"),
    alignItems: "center",
  },
  TickAgree: {
    width: "80%",
    height: hp("6%"),
    marginBottom: "7%",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },

  checkbox: {
    width: 20,
    height: 20,
    borderWidth: 1,
    borderRadius: 4,
    marginLeft: wp("5%"),
    borderColor: "#ff2e00",
    alignItems: "center",
    justifyContent: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  signup_res: state.main.signup_res,
});
export default connect(mapStateToProps, { createUser })(CustomerRegulation);
