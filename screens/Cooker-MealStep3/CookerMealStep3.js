import React, { useState, useEffect } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ImageBackground,
  Image,
  ActivityIndicator,
  AsyncStorage,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import ProfileDetail from "./components/ProfileDetail";
import {
  createMeal,
  getMealsByCooker,
} from "../../state-management/actions/features/Actions";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import { connect } from "react-redux";
import * as Location from "expo-location";

const CookerMealStep3 = (props) => {
  const [loading, setLoading] = useState(false);
  const [responseCode, setResponseCode] = useState();
  const [location, setLocation] = useState(null);
  let data = props.route.params?.data;
  const [cookerDetails, setCookerDetails] = useState("");

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg("Permission to access location was denied");
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation({
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
        distance: null,
      });
      const value = await AsyncStorage.getItem("userData");
      setCookerDetails(value);
    })();
  }, []);

  const onCreate = async () => {
    try {
      setLoading(true);
      const cookerData = await AsyncStorage.getItem("userData");
      let token = props.get_user_oauth_token?.access_token;
      let mealData = {
        useByDate: new Date(),
        available: true,
        quantity: data.quantity,
        price: data.price,
        geolocation: location,
        meal: {
          title: data.title,
          description: data.desc,
          timeForConsumption: data.timeForConsumption,
          categories: [data.category],
          cooker: JSON.parse(cookerData),
        },
      };
      props.createMeal(
        mealData,
        token,
        setLoading,
        setResponseCode,
        data?.mealPhoto
      );
    } catch (e) {
      console.log(e.message);
    }
  };

  useEffect(() => {
    if (responseCode == 201 || responseCode == 200) {
      refreshMeals();
      alert("Meal added succesfully !");
      props.navigation.navigate("CookerMeal");
    }
  }, [responseCode]);

  const refreshMeals = async () => {
    const value = await AsyncStorage.getItem("userSessionToken");
    await props.getMealsByCooker(value, setLoading);
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="black" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      {/* Header */}
      {/* <Header1 /> */}
      {/* Header */}

      <View style={styles.HeaderImg}>
        <ImageBackground
          style={{ width: "100%", height: "100%" }}
          source={{ uri: data.image }}
          resizeMode="cover"
        ></ImageBackground>
        <TouchableOpacity
          style={styles.backButton}
          onPress={() => props.navigation.goBack()}
        >
          <Ionicons name="arrow-back" size={rf(25)} color="white" />
        </TouchableOpacity>
      </View>
      {/* ProfileDetailComponent */}
      {cookerDetails?.length > 0 && (
        <ProfileDetail
          data={JSON.parse(cookerDetails)}
          cookerProfile={JSON.parse(cookerDetails)?.profileImg}
        />
      )}
      {/* ProfileDetailComponent */}
      <View style={styles.Discription}>
        <Text style={styles.Font1}>{data.title} </Text>
        <Text style={styles.Font2}>{data.category}</Text>
        <Text style={styles.Font3}>{data.desc}</Text>
        <Text style={styles.Font4}>{data.quantity} disponibles </Text>
        <Text style={styles.Font4}>
          {data.price}€{" "}
          <Text style={styles.Font5}>( part de {data.gram}g ) </Text>
        </Text>
        <Text style={styles.Font4}>
          {data.timeForConsumption} Time for consumption{" "}
        </Text>
      </View>
      <View style={styles.BtnWrapper}>
        <TouchableOpacity style={styles.Btn} onPress={onCreate}>
          <Text style={styles.BtnText}>Validate & put online</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },

  HeaderImg: {
    width: wp("100%"),
    height: hp("23%"),
  },
  Discription: {
    width: wp("100%"),
    height: hp("22%"),
    alignItems: "center",
    justifyContent: "space-between",
    marginTop: hp("3%"),
  },
  Font1: {
    fontSize: rf(20),
    fontWeight: "700",
    color: "#5E0D13",
  },
  Font2: {
    fontSize: rf(14),
    color: "#FF0000",
    fontWeight: "500",
  },
  Font3: {
    fontSize: rf(12),
    color: "#C22B2B",
    textAlign: "center",
    width: "70%",
  },
  Font4: {
    fontSize: rf(24),
    color: "#5E0D13",
    fontWeight: "700",
  },
  Font5: {
    fontSize: rf(12),
    color: "#5E0D13",
    fontWeight: "300",
  },
  BtnWrapper: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: hp("10%"),
  },
  Btn: {
    width: wp("90%"),
    height: hp("7%"),
    borderRadius: 10,
    backgroundColor: "#5E0D13",
    alignItems: "center",
    justifyContent: "center",
  },
  BtnText: {
    fontSize: rf(14),
    fontWeight: "700",
    color: "#fff",
  },
  backButton: {
    width: "100%",
    height: "100%",
    position: "absolute",
    top: hp("5%"),
    left: hp("2%"),
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_oauth_token: state.main.get_user_oauth_token,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { createMeal, getMealsByCooker })(
  CookerMealStep3
);
