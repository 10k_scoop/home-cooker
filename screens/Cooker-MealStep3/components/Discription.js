import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ImageBackground,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { Entypo, FontAwesome5, AntDesign } from "@expo/vector-icons";

export default function Discription({ navigation }) {
  return (
    <View style={styles.Heading}>
      <Text style={styles.headingFont}>
        Tartes aux pommes <Text style={styles.InnerheadingFont}>à 1,2 km</Text>
      </Text>
      <Text style={styles.discriptionFont}>
        Les ingrédients principaux sont : La pâte (farine de blé, sucre,beurre) et, surtout, les pommes.
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  Heading: {
    width: wp("90%"),
    height: hp("9%"),
    alignItems: "center",
    marginTop:20,
    marginHorizontal:wp('5%')
  },
  headingFont: {
    fontSize: rf(20),
    fontWeight: "700",
    marginBottom: 2,
    textAlign: "center",
  },
  InnerheadingFont: {
    fontSize: rf(12),
    fontWeight: "100",
    textAlign: "center",
  },
  discriptionFont: {
    fontSize: rf(12),
    color: "#C22B2B",
    textAlign: "center",
    fontWeight: "600",
    marginTop:5
  },
});
