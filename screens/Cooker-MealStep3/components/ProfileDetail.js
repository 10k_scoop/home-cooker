import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ImageBackground,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { Entypo } from "@expo/vector-icons";

export default function ProfileDetail(props) {
  return (
    <View style={styles.container}>
      <View style={styles.containerInner}>
        <View style={styles.Wrapper}>
          <View style={styles.ProfileRow}>
            <View style={styles.profile}>
              <ImageBackground
                style={{ width: "100%", height: "100%" }}
                source={{uri:`data:image/png;base64,${props?.cookerProfile}`}}
                resizeMode="cover"
              ></ImageBackground>
            </View>
            <Text style={styles.profileText}>{props.data?.username}</Text>
          </View>
          <View style={{width:'100%',height:'100%',position:'absolute',flexDirection:'row',left:120}}>
            <Text style={{ fontSize: rf(16), color: "#5E0D13", marginLeft: 10 }}>{props.data?.totalRating?.totalRatings}/5</Text>
            <Entypo name="star" size={rf(16)} color="#F4D639" />
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: wp("23%"),
    justifyContent: "center",
  },
  profile: {
    width: hp("13%"),
    height: hp("13%"),
    borderWidth: 2.5,
    borderRadius: 100,
    borderColor: "#F4D639",
    overflow: "hidden",
    marginLeft: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  containerInner: {
    width: "100%",
    height: "70%",
    flexDirection: "row",
    justifyContent: "center",
  },
  ProfileRow: {
    height: "100%",
    top: -48,
    alignItems: "center",
  },
  Wrapper: {
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  profileText: {
    fontSize: rf(16),
    color: "#5E0D13",
    marginLeft: 10,
    fontWeight: "700",
    marginTop:5
  },
});
