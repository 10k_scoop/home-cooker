import React from 'react';
import { ScrollView, StyleSheet, Text, TouchableOpacity, View, ImageBackground } from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from 'react-native-responsive-screen';
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { Entypo, FontAwesome, } from '@expo/vector-icons';

export default function AddOrderCard(props) {
    return (
        <View style={styles.CircleBar}>
            <TouchableOpacity style={styles.Plus} onPress={props.onPlusClick} >
                <Entypo name="plus" size={20} color="#fff" />
            </TouchableOpacity>

            <TouchableOpacity style={styles.Minus} onPress={props.onMinusClick}>
                <FontAwesome name="minus" size={20} color="#fff" />
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    CircleBar: {
        width: wp('100%'),
        height: hp('7%'),
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",

    },
    Plus: {
        width: hp('5%'),
        height: hp('5%'),
        borderWidth: 0.5,
        borderRadius: 100,
        justifyContent: "center",
        alignItems: "center",
        marginLeft: 3,
        marginRight: 3,
        backgroundColor: "#27a143",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 10,
    },
    Minus: {
        width: hp('5%'),
        height: hp('5%'),
        borderWidth: 0.5,
        borderRadius: 100,
        justifyContent: "center",
        alignItems: "center",
        marginLeft: 3,
        marginRight: 3,
        backgroundColor: "#d74753",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 10,
    }

});
