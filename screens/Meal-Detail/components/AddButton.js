import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ImageBackground,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";

export default function AddButton(props) {
  return (
    <TouchableOpacity style={styles.Btn} onPress={props.onAdd}>
      <Text style={{ fontSize: rf(16), fontWeight: "700", color: "#fff" }}>
        add to cart
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  Btn: {
    width: wp("28%"),
    height: hp("4.5%"),
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 0.5,
    borderRadius: 10,
    backgroundColor: "#80858c",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 10,
  },
});
