import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ImageBackground,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { Entypo, FontAwesome5, AntDesign } from "@expo/vector-icons";
import { getDistance } from 'geolib';

export default function Discription(props) {

  var dis = getDistance(
    { latitude: props.location?.coords.latitude, longitude: props.location?.coords.longitude },
    { latitude: props.data?.geolocation.latitude, longitude: props.data?.geolocation.longitude },
  );

  return (
    <View style={styles.Heading}>
      <Text style={styles.headingFont}>
        {props.data.meal.title + " "}
        <Text style={styles.InnerheadingFont}>
          à {Math.floor(dis / 1000)} km -
          {props.data.meal.categories[0]}
        </Text>
      </Text>
      <Text style={styles.discriptionFont}>
        {props.data.meal.description}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  Heading: {
    width: wp("90%"),
    alignItems: "center",
    marginTop: 20,
    marginHorizontal: wp('5%'),
    marginVertical:15
  },
  headingFont: {
    fontSize: rf(20),
    fontWeight: "700",
    marginBottom: 2,
    textAlign: "center",
  },
  InnerheadingFont: {
    fontSize: rf(12),
    fontWeight: "100",
    textAlign: "center",
  },
  discriptionFont: {
    fontSize: rf(12),
    color: "#C22B2B",
    textAlign: "center",
    fontWeight: "600",
    marginTop: 5
  },
});
