import React, { useState, useEffect } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ImageBackground,
  Image,
  ActivityIndicator,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import {
  AntDesign,
  MaterialCommunityIcons,
  MaterialIcons,
  Ionicons,
  Fontisto,
  Entypo,
} from "@expo/vector-icons";
import ProfileDetail from "./components/ProfileDetail";
import Header1 from "../../Components/Header1";
import Discription from "./components/Discription";
import AddOrderCard from "./components/AddOrderCard";
import AddButton from "./components/AddButton";
import MenuPicCard from "./components/MenuPicCard";
import BottomMenu from "../../Components/BottomMenu";
import { connect } from "react-redux";
import { logout } from "../../state-management/actions/auth/authActions";
import { getMealsByReferrence } from "../../state-management/actions/features/Actions";
const MealDetail = (props) => {
  let data = props.route.params.data;
  let allMeals = props.route.params?.allMeals;
  var base64Image =
    "data:image/png;base64," + data?.meal?.photos[0]?.image?.data;
  const [orderItemCount, setOrderItemCount] = useState(0);
  const [meal, setMeal] = useState();
  const [loading, setLoading] = useState(true);

  let CookerProfile = `data:image/png;base64,${data?.meal?.cooker?.profileImg}`;

  useEffect(() => {
    props.getMealsByReferrence(
      props.get_user_oauth_token?.access_token,
      data.reference,
      setLoading
    );
  }, []);

  useEffect(() => {
    if (props.get_meals_by_reference != "") {
      setMeal(JSON.parse(props.get_meals_by_reference));
    } else {
      //setMeals({content:[]})
    }
  }, [props]);

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator color="black" size="large" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <ScrollView>
        {/* Header */}
        <Header1
          locationName={
            props.route.params.locationName != null
              ? props.route.params.locationName
              : []
          }
        />
        {/* Header */}

        <View style={styles.HeaderImg}>
          <ImageBackground
            style={{ width: "100%", height: "100%" }}
            source={{ uri: base64Image }}
            resizeMode="cover"
          ></ImageBackground>
        </View>
        {/* ProfileDetailComponent */}
        <ProfileDetail data={data} CookerProfile={CookerProfile} />
        {/* ProfileDetailComponent */}
        {/* Discription */}
        <Discription data={data} location={props.route.params.location} />
        {/* Discription */}
        <View style={styles.Discription2}>
          <Text style={{ fontSize: rf(30), fontWeight: "700" }}>
            {data?.price}€{" "}
            <Text style={{ fontSize: rf(14), fontWeight: "600" }}>
              ( part de 100g )
            </Text>
          </Text>
        </View>
        {/* AddOrderCard */}
        {props.route.params?.type != "cooker" && (
          <AddOrderCard
            onPlusClick={() => setOrderItemCount(orderItemCount + 1)}
            onMinusClick={() =>
              orderItemCount >= 1 ? setOrderItemCount(orderItemCount - 1) : null
            }
          />
        )}
        {/* AddOrderCard */}
        {props.route.params?.type != "cooker" && (
          <View style={styles.Discription2}>
            <Text style={{ fontSize: rf(24), fontWeight: "700" }}>
              {orderItemCount} parts : total {meal?.price * orderItemCount}€
            </Text>
          </View>
        )}

        {props.route.params?.type == "cooker" && (
          <View
            style={{
              flexDirection: "row",
              marginHorizontal: wp("5%"),
              alignItems: "center",
              marginTop: hp("2%"),
            }}
          >
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Image
                source={require("../../assets/Sell.png")}
                style={{ width: 60, height: 61 }}
              />
              <Text
                style={{ fontSize: rf(15), fontWeight: "400", marginLeft: 10 }}
              >
                {meal?.orders.reduce((a, v) => (a = a + v.quantity), 0)}/
                {meal?.quantity} parts
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginLeft: 10,
              }}
            >
              <Image
                source={require("../../assets/Win.png")}
                style={{ width: 60, height: 61 }}
              />
              <Text
                style={{
                  fontSize: rf(28),
                  fontWeight: "600",
                  marginLeft: 10,
                  color: "#FF1E00",
                }}
              >
                {meal?.orders.length * meal?.quantity} €
              </Text>
            </View>
          </View>
        )}

        {props.route.params?.type != "cooker" && (
          <View style={{ width: wp("100%"), alignItems: "center" }}>
            {meal?.orders.length != meal?.quantity ? (
              <AddButton
                onAdd={() =>
                  orderItemCount >= 1
                    ? props.navigation.navigate("CustomerCart", {
                        data: meal,
                        location: props.route.params.location,
                        locationName: props.route.params.locationName,
                        orderDetails: {
                          orderItemCount: orderItemCount,
                        },
                      })
                    : alert("Order limit is atleast 1 !")
                }
              />
            ) : (
              <Text
                style={{ color: "red", fontSize: rf(20), fontWeight: "bold" }}
              >
                SOLD OUT !
              </Text>
            )}
          </View>
        )}
        <View style={styles.Discription3}>
          <Text style={{ fontSize: rf(12), color: "grey" }}>
            Autres plats de {data?.meal?.cooker?.username}
          </Text>
        </View>
        <View style={styles.MenuCards}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={styles.innerMenuCards}>
              {allMeals?.map((item, index) => {
                let imgUrl = `data:image/png;base64,${item?.meal?.photos[0]?.image?.data}`;
                if (
                  item?.meal?.cooker?.username ==
                    data?.meal?.cooker?.username &&
                  data?.reference != item?.reference
                ) {
                  return (
                    <MenuPicCard
                      location={props?.route?.params?.location}
                      locationName={props?.route?.params?.locationName}
                      navigation={props?.navigation}
                      key={index}
                      img={imgUrl}
                      data={item}
                      type="cooker"
                      allMeals={allMeals}
                    />
                  );
                }
              })}
              {meal?.meal.photos.length > 3 && (
                <View style={{ height: "100%", justifyContent: "center" }}>
                  <Entypo name="chevron-right" size={rf(25)} color="black" />
                </View>
              )}
            </View>
          </ScrollView>
        </View>
      </ScrollView>
      <BottomMenu
        navigation={props.navigation}
        userProfile={JSON.parse(props.get_user_details)?.profil}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  BottomMenu: {
    width: wp("100%"),
    height: Platform.OS == "ios" ? hp("8%") : hp("7%"),
    backgroundColor: "#222",
    position: "absolute",
    bottom: 0,
    borderTopLeftRadius: 7,
    borderTopRightRadius: 7,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingBottom: Platform.OS == "ios" ? 15 : 5,
  },
  IconCircle: {
    width: hp("5%"),
    height: hp("5%"),
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#fff",
    overflow: "hidden",
  },
  HeaderImg: {
    width: wp("100%"),
    height: hp("23%"),
  },
  Discription2: {
    width: wp("100%"),
    height: hp("5%"),
    alignItems: "center",
  },
  Discription3: {
    width: wp("100%"),
    height: hp("2%"),
    justifyContent: "center",
    paddingHorizontal: 10,
    marginTop: 20,
  },
  MenuCards: {
    width: wp("100%"),
    height: hp("17%"),
    marginBottom: "20%",
    flexDirection: "row-reverse",
    alignItems: "center",
    paddingHorizontal: 5,
  },
  innerMenuCards: {
    width: "100%",
    flexDirection: "row-reverse",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_oauth_token: state.main.get_user_oauth_token,
  get_user_details: state.main.get_user_details,
  get_meals_by_reference: state.main.get_meals_by_reference,
});
export default connect(mapStateToProps, { logout, getMealsByReferrence })(
  MealDetail
);
