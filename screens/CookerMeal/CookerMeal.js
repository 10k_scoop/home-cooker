import React, { useEffect, useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  BackHandler,
  Alert,
  ActivityIndicator,
  Platform,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import Header1 from "../../Components/Header1";
import BottomMenu from "../../Components/BottomMenu";
import {
  getUserOAuthToken,
  logout,
} from "../../state-management/actions/auth/authActions";
import { connect } from "react-redux";
import * as Location from "expo-location";
import {
  getMealsByCooker,
  removeMeal,
} from "../../state-management/actions/features/Actions";
import MealListCard from "../../Components/MealListCard";
import { decode as atob, encode as btoa } from "base-64";
import { AsyncStorage } from "react-native";

const CookerMeal = (props) => {
  const [loading, setLoading] = useState(true);
  const [meals, setMeals] = useState([]);
  const [location, setLocation] = useState(null);
  const [locationName, setLocationName] = useState(null);
  const [refresh, setRefresh] = useState(false);
  const [orders, setOrders] = useState(0);

  useEffect(() => {
    (async () => {
      const value = await AsyncStorage.getItem("userSessionToken");
      if (value !== null) {
        props.getMealsByCooker(value, setLoading, setMeals);
        //console.log(value)
      } else {
        props.getMealsByCooker(
          props.get_user_oauth_token?.access_token,
          setLoading,
          setMeals
        );
      }
    })().catch((e) => {});
  }, []);

  //Fetch current location of user
  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        alert("Permission to access location was denied");
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      let locationCoords = {
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
      };
      let locationName = await Location.reverseGeocodeAsync(locationCoords);
      setLocation(location);
      setLocationName(locationName);
    })();
  }, [props]);
  //Fetch current location of user

  useEffect(() => {
    if (props.get_meals_by_cooker != "") {
      setMeals(JSON.parse(props.get_meals_by_cooker));
      setOrders(
        JSON.parse(props.get_meals_by_cooker)?.content[0]?.orders.length
      );
    } else {
      //setMeals({content:[]})
    }
  }, [props, setRefresh]);


  const baseUrl = "https://allcooks-backend.herokuapp.com";

  //Remove a meal
  const onRemoveMeal = (item) => {
    setLoading(true);
    //console.log(props.get_user_oauth_token.access_token);
    try {
      fetch(`${baseUrl}` + "/instanceMeals/disable/" + item.reference, {
        method: "POST",
        headers: {
          Authorization: "Bearer " + props.get_user_oauth_token.access_token,
          "Content-Type": "application/json",
        },
      })
        .then((response) => {
          const statusCode = response.status;
          console.log(statusCode);
        })
        .then((json) => {
          props.getMealsByCooker(
            props.get_user_oauth_token?.access_token,
            setLoading,
            setMeals
          );
        })
        .catch((error) => {
          console.log(error.message);
          setLoading(false);
          alert("Something went wrong try again !");
        });
    } catch (e) {
      console.log(e.message);
      setLoading(false);
      alert("Something went wrong try again !");
    }
  };
  const onRefresh = () => {
    setLoading(true);
    props.getMealsByCooker(
      props.get_user_oauth_token?.access_token,
      setLoading,
      setMeals
    );
  };

  if (loading || location==null) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator color="black" size="large" />
      </View>
    );
  }
  //console.log(meals);
  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {/* Header */}
        <Header1
          orderDetails={meals}
          orders={orders}
          locationName={locationName != null ? locationName : []}
          refresh={true}
          onRefreshPress={onRefresh}
          onCartPress={() =>
            props.navigation.navigate("CookerMealOrder", {
              ordersCount: orders,
              coords: location,
              location: locationName,
              order: meals?.content[0]?.orders,
            })
          }
        />
        {/* Header */}

        {/* Cards */}
        {meals?.content?.length > 0 && (
          <View style={styles.Cards}>
            {meals?.content?.map((item, index) => {
              let d1 = new Date(item?.useByDate);
              let d2 = new Date();
              let hours = Math.abs(d1 - d2) / 36e5;
              let mealRemainingTime = Math.floor(
                (hours / item?.meal?.timeForConsumption) * 100
              );
              //console.log(hours+" "+Platform.OS);
              return (
                <MealListCard
                  onRemovePress={() => onRemoveMeal(item)}
                  data={item}
                  type="cooker"
                  location={location?location:{}}
                  key={index}
                  hours={Math.floor(hours)}
                  MRT={mealRemainingTime}
                  onpress={() =>
                    props.navigation.navigate("MealDetail", {
                      data: item,
                      allMeals: meals?.content,
                      location: location,
                      locationName: locationName,
                      type: "cooker",
                    })
                  }
                />
              );
            })}
          </View>
        )}
        {/* Cards */}
      </ScrollView>
      <TouchableOpacity
        style={styles.addMealBtn}
        onPress={() => props.navigation.navigate("CookerMealStep1")}
      >
        <Text style={styles.addMealBtnText}>Add a Meal</Text>
      </TouchableOpacity>
      <BottomMenu
        navigation={props.navigation}
        userProfile={JSON.parse(props.get_user_details)?.profil}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Cards: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "35%",
    flexDirection:'column-reverse'
  },
  BottomMenu: {
    width: wp("100%"),
    height: Platform.OS == "ios" ? hp("8%") : hp("7%"),
    backgroundColor: "#222",
    position: "absolute",
    bottom: 0,
    borderTopLeftRadius: 7,
    borderTopRightRadius: 7,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingBottom: Platform.OS == "ios" ? 15 : 5,
  },
  IconCircle: {
    width: hp("5%"),
    height: hp("5%"),
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#fff",
  },
  addMealBtn: {
    width: wp("40%"),
    height: hp("5%"),
    backgroundColor: "#5E0D13",
    borderRadius: 10,
    position: "absolute",
    bottom: hp("10%"),
    alignItems: "center",
    justifyContent: "center",
  },
  addMealBtnText: {
    color: "#fff",
    fontSize: rf(15),
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_oauth_token: state.main.get_user_oauth_token,
  get_user_details: state.main.get_user_details,
  get_meals_by_cooker: state.main.get_meals_by_cooker,
});
export default connect(mapStateToProps, {
  getUserOAuthToken,
  getMealsByCooker,
  logout,
  removeMeal,
})(CookerMeal);
