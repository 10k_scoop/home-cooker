import React, { useState } from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import Header from "../../Components/Header";
import TextField from "../../Components/TextField";
import Button from "../../Components/Button";
import { Picker } from "@react-native-picker/picker";
import CountryList from "../../Components/CountryList";

export default function CookerLocation(props) {
  const [country, setCountry] = useState();
  const [number, setNumber] = useState("");
  const [street, setStreet] = useState("");
  const [zipCode, setZipCode] = useState("");

  const data = props.route.params;

  const onNext = () => {
    if (country == null || number == "") {
      alert("Fill all fields");
    } else {
      props.navigation.navigate("CookerExperience", {
        email: data.email,
        pass: data.pass,
        age: data.age,
        gender: data.gender,
        username: data.username,
        oAuthToken: data.oAuthToken,
        phoneNumber:number,
        address: {
          number: number,
          street: street,
          zipCode: zipCode,
          country: country,
          state: null,
          zipCode: 93000,
        },
      });
    }
  };

  return (
    <View style={styles.container}>
      <Header Title="Location" OnBackPress={() => props.navigation.goBack()} />
      <ScrollView style={{ marginTop: 15 }}>
        <View style={styles.inputWrapper}>
          <Text style={styles.label}>Country</Text>
          <View style={styles.picker}>
            <Picker
              selectedValue={country}
              onValueChange={(itemValue, itemIndex) => setCountry(itemValue)}
              itemStyle={{ fontSize: rf(13) }}
            >
              <Picker.Item label="Select Country" value={null} />
              {CountryList.map((item, index) => {
                return (
                  <Picker.Item
                    key={index}
                    label={item.name}
                    value={item.name}
                  />
                );
              })}
            </Picker>
          </View>
        </View>
        <View style={styles.inputWrapper}>
          <Text style={styles.label}>Number</Text>
          <TextField
            Txt="Number"
            placeholder=""
            onChangeText={(val) => setNumber(val)}
          />
        </View>
        <View style={styles.inputWrapper}>
          <Text style={styles.label}>Street</Text>
          <TextField
            Txt="Street"
            placeholder=""
            onChangeText={(val) => setStreet(val)}
          />
        </View>
        <View style={styles.inputWrapper}>
          <Text style={styles.label}>ZipCode</Text>
          <TextField
            Txt="ZipCode"
            placeholder=""
            onChangeText={(val) => setZipCode(val)}
          />
        </View>

        <View style={styles.BtnWrapper}>
          <Button BtnTxt="Next" onPress={() => onNext()} />
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  BtnWrapper: {
    height: hp("15%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
    alignItems: "center",
  },
  inputWrapper: {
    width: wp("100%"),
    marginVertical: hp("0.3%"),
  },
  label: {
    fontSize: rf(12),
    fontWeight: "500",
    paddingHorizontal: wp("5%"),
    left: 5,
  },
  picker: {
    borderWidth: 1,
    paddingHorizontal: "5%",
    justifyContent: "center",
    width: wp("90%"),
    height: hp("7%"),
    marginHorizontal: wp("5%"),
    overflow: "hidden",
    marginTop: 10,
    borderColor: "#e5e5e5",
    borderRadius: 10,
  },
});
