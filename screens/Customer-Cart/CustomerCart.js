import React, { useEffect, useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  Image,
  ActivityIndicator,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { EvilIcons, MaterialIcons, Ionicons } from "@expo/vector-icons";
import OrderCard from "./components/OrderCard";
import AddOrder from "./components/AddOrder";
import PaymentBtn from "./components/PaymentBtn";
import RevenierBtn from "./components/RevenierBtn";
import BottomMenu from "../../Components/BottomMenu";
import { connect } from "react-redux";
import { OrderMeal } from "../../state-management/actions/features/Actions";
import { logout } from "../../state-management/actions/auth/authActions";
import { CardField, useConfirmPayment } from "@stripe/stripe-react-native";
import { StripeProvider } from "@stripe/stripe-react-native";

const API_URL = "http://192.168.100.163:3000";

const CustomerCart = (props) => {
  let data = props.route.params?.data;
  const [loading, setLoading] = useState(false);
  const [responseCode, setResponseCode] = useState();
  const [cardDetails, setCardDetails] = useState();
  const [loader, setLoader] = useState(false);
  const [paymentDone, setPaymentDone] = useState(false);
  const { confirmPayment, ploading } = useConfirmPayment();

  const onOrder = () => {
    try {
      setLoading(true);
      let d = {
        instanceMealReference: data.reference,
        shopper: data.meal.cooker.username,
        quantity: props.route.params.orderDetails.orderItemCount,
      };
      props.OrderMeal(
        props.get_user_oauth_token.access_token,
        d,
        setLoading,
        setResponseCode
      );
    } catch (e) {
      console.log(e.message);
    }
  };

  useEffect(() => {
    if (responseCode == 201 || responseCode == 200) {
      alert("Order Placed Successfully ");
      props.navigation.navigate("MealList");
    } else if (
      responseCode == 401 ||
      responseCode == 400 ||
      responseCode == 501
    ) {
      alert("Something went wrong");
    } else {
    }
  }, [responseCode]);

  const fetchPaymentIntentClientSecret = async () => {
    try {
      const response = await fetch(`${API_URL}/create-payment-intent`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          data: {
            email: data?.meal?.cooker?.email,
            amount: data?.price,
          },
          orderDetails: props?.route?.params?.orderDetails,
        }),
      });
      const { clientSecret, error } = await response.json();
      return { clientSecret, error };
    } catch (e) {
      console.log(e.message);
    }
  };

  const handlePayPress = async () => {
    setLoading(true);
    //1.Gather the customer's billing information (e.g., email)
    if (!cardDetails?.complete) {
      Alert.alert("Please enter Complete card details and Email");
      return;
    }
    const billingDetails = {
      email: data?.meal?.cooker?.email,
      amount: parseInt(data?.price),
      instanceMealReference: data?.reference,
      shopper: data?.meal?.cooker?.username,
      quantity: props.route.params?.orderDetails?.orderItemCount,
    };
    //2.Fetch the intent client secret from the backend
    try {
      setLoader(true);
      const { clientSecret, error } = await fetchPaymentIntentClientSecret();
      //2. confirm the payment
      if (error) {
        setLoader(false);
        console.log("Unable to process payment");
      } else {
        const { paymentIntent, error } = await confirmPayment(clientSecret, {
          type: "Card",
          billingDetails: billingDetails,
        });
        if (error) {
          setLoader(false);
          alert(`Payment Confirmation Error ${error.message}`);
        } else if (paymentIntent) {
          onOrder();
        }
      }
    } catch (e) {
      setLoader(false);
      console.log(e.message);
    }
  };

  return (
    <StripeProvider publishableKey="pk_test_51Kmkw8FmRzRvN6saPIcCQqiK1bfFBGR6Q0yb0jElAiyens59wLivEQFUNHptGRga6vIPi1Yd2EjkH2HHVTLQDAw600coOtYywT">
      <View style={styles.container}>
        {loading && (
          <View style={styles.loader}>
            <View style={[styles.loader, { backgroundColor: "white" }]}></View>
            <Text
              style={{
                fontSize: 15,
                marginBottom: 20,
                fontWeight: "bold",
                zIndex: 99999999999999999,
              }}
            >
              Processing your request please wait....
            </Text>
            <ActivityIndicator
              color="black"
              size="large"
              style={{ zIndex: 99999999999999999 }}
            />
          </View>
        )}
        <KeyboardAvoidingView
          behavior={Platform.OS === "ios" ? "padding" : "height"}
        >
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.innerWrapper}>
              <View style={styles.SearchBar}></View>
              <View style={styles.heading}>
                <Text style={{ fontSize: rf(24), fontWeight: "700" }}>
                  Mon Panier
                </Text>
              </View>
              {/* OrderCard */}
              <OrderCard data={data} />
              {/* OrderCard */}

              {/* <View style={styles.AddOrder}>
        <AddOrder />
      </View> */}
              <View style={styles.BorderBottom}></View>
              <View style={styles.TotalAmount}>
                <Text style={{ fontSize: rf(22), color: "#5E0D13" }}>
                  Total
                </Text>
                <Text
                  style={{
                    fontSize: rf(22),
                    fontWeight: "700",
                    color: "#5E0D13",
                  }}
                >
                  {data.price * props.route.params.orderDetails.orderItemCount}€
                </Text>
              </View>
              {/* <PaymentBtn onPress={() => redirectToPay()} title="Order Now" /> */}
              <View>
                <Text style={styles.headerTitle}>Payment Method</Text>
                <Text style={styles.title}>Enter Card details</Text>
                <CardField
                  postalCodeEnabled={true}
                  placeholder={{
                    number: "4242 4242 4242 4242",
                  }}
                  cardStyle={styles.card}
                  style={styles.cardContainer}
                  onCardChange={(cardDetails) => {
                    setCardDetails(cardDetails);
                  }}
                />
                <TouchableOpacity
                  style={styles.addPaymentMethodButton}
                  disabled={ploading}
                  onPress={handlePayPress}
                >
                  <Text style={styles.addPaymentMethodButtonText}>Pay now</Text>
                </TouchableOpacity>
              </View>
              {/* <RevenierBtn onClick={() => props.navigation.navigate("CustomerEmptyCart")} /> */}
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
        <BottomMenu
          navigation={props.navigation}
          userProfile={JSON.parse(props.get_user_details)?.profil}
        />
      </View>
    </StripeProvider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  SearchBar: {
    width: wp("90%"),
    height: hp("12%"),
    justifyContent: "flex-end",
  },
  InnerSearchBar: {
    width: "100%",
    height: "50%",
    borderWidth: 0.5,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 100,
    paddingHorizontal: 5,
  },
  TextField: {
    width: "90%",
    height: "100%",
    fontSize: rf(18),
    paddingHorizontal: 5,
  },
  heading: {
    width: wp("90%"),
    height: hp("10%"),
    justifyContent: "center",
    alignItems: "center",
  },
  AddOrder: {
    width: wp("90%"),
    height: hp("6%"),
    justifyContent: "center",
  },
  BorderBottom: {
    width: wp("90%"),
    height: hp("8%"),
    borderBottomWidth: 2,
    borderColor: "#e5e5e5",
  },
  TotalAmount: {
    width: wp("90%"),
    height: hp("8%"),
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: "8%",
    alignItems: "center",
    marginBottom: "2%",
  },
  addPaymentMethodButton: {
    width: wp("90%"),
    height: hp("5%"),
    backgroundColor: "#DCA007",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp("3%"),
    borderRadius: 5,
    paddingHorizontal: wp("5%"),
    alignSelf: "center",
  },
  addPaymentMethodButtonText: {
    fontSize: rf(18),
    color: "#fff",
    marginLeft: 10,
  },
  cardContainer: {
    height: 50,
    marginVertical: 10,
  },
  card: {
    backgroundColor: "#efefef",
    borderRadius: 10,
  },
  loader: {
    position: "absolute",
    zIndex: 99999999,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: hp("100%"),
  },
  title: {
    fontSize: rf(18),
    marginLeft: 10,
    color: "#A59F9F",
    marginVertical: 10,
  },
  headerTitle: {
    fontSize: rf(22),
    marginLeft: 10,
    color: "#222",
    marginTop: 10,
    marginBottom: hp("3%"),
    fontWeight: "600",
    textAlign: "center",
  },
  innerWrapper: {
    flex: 1,
    alignItems: "center",
    height: hp("100%"),
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_oauth_token: state.main.get_user_oauth_token,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { logout, OrderMeal })(CustomerCart);
