import React from 'react';
import { ScrollView, StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from 'react-native-responsive-screen';
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from '@expo/vector-icons';

export default function OrderCard(props) {
    console.log(props?.data?.meal?.title)
    var base64Image =
    "data:image/png;base64," + props?.data?.meal?.photos[0]?.image?.data;
    return (
        <View style={styles.Card}>
            <View style={styles.Pic}>
                <Image
                    style={{ width: "100%", height: "100%" }}
                    source={{uri:base64Image}}
                    resizeMode="cover"
                />
            </View>
            <View style={styles.Details}>
                <View style={styles.InnerDetail1}>
                    <Text style={{ fontSize: rf(18), fontWeight: "700", color: "#5E0D13" }}>{props?.data?.meal?.title}</Text>
                    <Text style={{ fontSize: rf(12), color: "#C22B2B" }}>100 grammer</Text>
                    <Text style={{ fontSize: rf(12), color: "green" }}>En Stock</Text>
                </View>
                <View style={styles.InnerDetail2}>
                    <Text style={{ fontSize: rf(16), color: "#5E0D13" }}>Vendu par {props?.data?.meal?.cooker?.username}</Text>
                </View>
            </View>
            <View style={styles.DletIcon}>
                <TouchableOpacity>
                    <AntDesign name="delete" size={rf(25)} color="black" />
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    Card: {
        width: wp('90%'),
        height: hp('18%'),
        flexDirection: "row",
        alignItems: "center"
    },
    Pic: {
        width: "35%",
        height: "90%",
        borderRadius: 10,
        overflow: "hidden"
    },
    Details: {
        width: "53%",
        height: "90%",
        paddingHorizontal: 12
    },
    InnerDetail1: {
        width: "100%",
        justifyContent: "space-around"
    },
    InnerDetail2: {
        width: "100%",
        height: "50%",
        justifyContent: "flex-end",
        paddingVertical: 5
    },
    DletIcon: {
        width: "15%",
        height: "90%",
        alignItems: "center"
    }


});
