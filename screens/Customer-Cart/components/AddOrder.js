import React, { useState } from 'react';
import { ScrollView, StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from 'react-native-responsive-screen';
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from '@expo/vector-icons';

export default function AddOrder(props) {
    const [value, setValue] = useState(1)

    const increaseValue = () => {
        setValue(value +1)

    }
    const decreaseValue = () => {
        if(value>=1){
            setValue(value -1)
        }
    }



    return (
        <View style={styles.Card}>
            <TouchableOpacity style={styles.Decrease} onPress={decreaseValue}>
                <AntDesign name="minus" size={rf(18)} color="black" />
            </TouchableOpacity>
            <View style={styles.Quantity}>
                <Text>{value}</Text>
            </View>
            <TouchableOpacity style={styles.Increase} onPress={increaseValue}>
                <AntDesign name="plus" size={rf(16)} color="black" />
            </TouchableOpacity>

        </View>


    );
}

const styles = StyleSheet.create({
    Card: {
        width: wp('30%'),
        height: hp('4%'),
        flexDirection: "row",
        borderWidth: 0.5,
        borderRadius: 2,
        overflow: "hidden"

    },
    Decrease: {
        width: "33%",
        height: "100%",
        borderRightWidth: 0.5,
        backgroundColor: "#E5E5E5",
        justifyContent: "center",
        alignItems: "center"
    },
    Quantity: {
        width: "34%",
        height: "100%",
        justifyContent: "center",
        alignItems: "center"
    },
    Increase: {
        width: "33%",
        height: "100%",
        borderLeftWidth: 0.5,
        backgroundColor: "#E5E5E5",
        alignItems: "center",
        justifyContent: "center"
    }


});
