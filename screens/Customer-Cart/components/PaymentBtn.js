import React, { useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  Modal,
  View,
  Image,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { Entypo, AntDesign } from "@expo/vector-icons";

export default function PaymentBtn(props) {
  const [pop, setPop] = useState(false);

  return (
    <View>
      <TouchableOpacity style={styles.Card} onPress={props.onPress}>
        <Text style={{ fontSize: rf(18), color: "#fff", fontWeight: "700" }}>
          {props.title}
        </Text>
      </TouchableOpacity>
        {/* {pop == true ? (
        <Modal transparent={true}>
          <View style={{ flex: 1, backgroundColor: "#000000aa" }}>
            <View style={styles.PopView}>
              <View style={styles.FirstRow}>
                <Text style={{ fontSize: rf(16), marginRight: 10 }}>
                  Choisir un moyen de paiement
                </Text>
                <TouchableOpacity onPress={() => setPop(false)}>
                  <Entypo name="cross" size={rf(22)} color="grey" />
                </TouchableOpacity>
              </View>
              <View style={styles.SecondRow}>
                <View style={styles.CardImage}>
                  <Image
                    style={{ width: "100%", height: "100%" }}
                    source={require("../../../assets/Card1.png")}
                    resizeMode="cover"
                  />
                </View>
                <View style={styles.CardName}>
                  <Text style={{ fontSize: rf(18), color: "grey" }}>
                    Carte bancaire
                  </Text>
                </View>
                <TouchableOpacity onPress={props.onClick}>
                  <AntDesign name="right" size={rf(20)} color="grey" />
                </TouchableOpacity>
              </View>
              <View style={styles.ThirdRow}>
                <View style={styles.CardImage}>
                  <Image
                    style={{ width: "100%", height: "100%" }}
                    source={require("../../../assets/Card1.png")}
                    resizeMode="cover"
                  />
                </View>
                <View style={styles.CardName}>
                  <Text style={{ fontSize: rf(18), color: "grey" }}>
                    Paypal
                  </Text>
                </View>
                <TouchableOpacity onPress={props.onAdd}>
                  <AntDesign name="right" size={rf(20)} color="grey" />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      ) : null} */}
    </View>
  );
}

const styles = StyleSheet.create({
  Card: {
    width: wp("85%"),
    height: hp("7%"),
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#DCA007",
    marginBottom: 10,
  },
  PopView: {
    width: "100%",
    height: "25%",
    position: "absolute",
    bottom: 0,
    backgroundColor: "#fff",
    borderTopLeftRadius: 14,
    borderTopRightRadius: 14,
    alignItems: "center",
  },
  FirstRow: {
    width: "90%",
    height: "20%",
    borderBottomWidth: 2,
    borderColor: "#e5e5e5",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  SecondRow: {
    width: "90%",
    height: "40%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  CardImage: {
    width: "16%",
    height: "45%",
    borderRadius: 5,
    overflow: "hidden",
  },
  CardName: {
    width: "70%",
    height: "45%",
    justifyContent: "center",
    paddingHorizontal: 10,
  },
  ThirdRow: {
    width: "90%",
    height: "40%",
    flexDirection: "row",
    justifyContent: "center",
  },
});
