import React, { useEffect } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  TextInput,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import BottomMenu from "../../Components/BottomMenu";
import Cards from "./components/Cards";
import { connect } from "react-redux";
import { getUserOAuthToken, logout } from "../../state-management/actions/auth/authActions";
const CustomerCommandes = (props) => {

  useEffect(()=>{
    console.log(props.get_user_details)
  },[])


  return (
    <View style={styles.Container}>
      <View style={styles.title}>
        <Text style={styles.titleText}>Mes commandes</Text>
      </View>
      <Cards
        title="En cours"
        Order="Order n°4532"
        amount="8€"
        img={require("../../assets/MealPic.jpg")}
      />
      <Cards
        title="Terminées"
        Order="Order n°532"
        amount="15€"
        img={require("../../assets/MealPic.jpg")}
      />
      <Cards
        title="Terminées"
        Order="Order n°5432"
        amount="25€"
        img={require("../../assets/MealPic.jpg")}
      />

      <BottomMenu />
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  title: {
    width: wp("90%"),
    height: hp("15%"),
    justifyContent: "flex-end",
    paddingVertical: hp("2%"),
    marginBottom: "4%",
  },
  titleText: {
    fontSize: rf(26),
    fontWeight: "400",
    color: "#5E0D13",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_oauth_token: state.main.get_user_oauth_token,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, {
  getUserOAuthToken,
  logout,
})(CustomerCommandes);
