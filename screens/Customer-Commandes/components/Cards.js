import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  TextInput,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";

export default function Cards(props) {
  return (
    <View style={styles.Container}>
      <View style={styles.FirstRow}>
        <Text style={styles.FirstRowText}>{props.title}</Text>
      </View>
      <View style={styles.SecondRow}>
        <View style={styles.Detail}>
          <Text style={styles.DetailFont1}>{props.Order}</Text>
          <View style={styles.wrapper}>
            <Text style={styles.DetailFont2}>Total</Text>
            <Text style={styles.DetailFont2}>{props.amount}</Text>
          </View>
        </View>
        <View style={styles.Img}>
          <View style={styles.Image}>
            <Image
              source={props.img}
              style={{ width: "100%", height: "100%" }}
              resizeMode="cover"
            />
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    width: wp("90%"),
    height: hp("15%"),
    borderBottomWidth: 1.5,
    borderColor: "#e5e5e5",
    justifyContent: "space-around",
    marginBottom: "2%",
  },
  FirstRow: {
    width: "18%",
    height: "23%",
    backgroundColor: "#DCA007",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: "5%",
  },
  FirstRowText: {
    fontSize: rf(12),
  },
  SecondRow: {
    width: "100%",
    height: "70%",
    flexDirection: "row",
  },
  Detail: {
    width: "60%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  DetailFont1: {
    fontSize: rf(18),
    color: "#5E0D13",
  },
  DetailFont2: {
    fontSize: rf(24),
    fontWeight: "700",
    color: "#5E0D13",
  },
  Img: {
    width: "40%",
    height: "100%",
    alignItems: "flex-end",
    justifyContent: "center",
    paddingHorizontal: "5%",
  },
  Image: {
    width: wp("17%"),
    height: wp("17%"),
    borderRadius: 10,
    overflow: "hidden",
  },
  wrapper: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-evenly",
    paddingHorizontal: wp("5%"),
  },
});
