import React, { useEffect, useState } from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Header from "../../Components/Header";
import Button from "../../Components/Button";
import { Entypo } from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";

export default function CookerProfile(props) {
  const [profileImg, setProfileImg] = useState(null);
  const data = props.route.params;
  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const { status } =
          await ImagePicker.requestMediaLibraryPermissionsAsync();
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.IMAGE,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setProfileImg(result);
    }
  };

  const onNext = () => {
    let newData={
      email: data.email,
      password: data.pass,
      age: data.age,
      sex: data.gender,
      username: data.username,
      oAuthToken: data.oAuthToken,
      experience: data.experience,
      desc: data.desc,
      address: {
        country: data.address.country,
        number: data.address.number,
        street: data.address.street,
        zipCode: data.address.zipCode,
        state: null,
      },
      phoneNumber:data.address.number,
      profileImg: profileImg,
      profil: "COOKER",
      totalRating: {
        totalComments: 0,
        totalRatings: 0,
        avgScore: 0.0,
      },
      meals: null,
      favoriteMeals: null,
    }
    if (profileImg == null) {
      alert("Fill all fields");
    } else {
      props.navigation.navigate("CookerRegulation",{data:newData} );
    }
  };
  return (
    <View style={styles.container}>
      <Header Title="Photo" OnBackPress={() => props.navigation.goBack()} />
      <View style={{ height: hp("9%") * 5.3 }}>
        <View style={styles.TextWrapper}>
          <Text style={{ fontSize: rf(14), fontWeight: "600" }}>
            Profile Picture
          </Text>
        </View>
        <View style={styles.ProfilePicWrapper}>
          <TouchableOpacity style={styles.ProfilePicBody} onPress={pickImage}>
            {profileImg && (
              <Image
                source={{ uri: profileImg?.uri }}
                style={{ width: "100%", height: "100%" }}
                resizeMode="cover"
              />
            )}
            <Entypo
              name="camera"
              size={rf(35)}
              color="black"
              style={styles.cameraIcon}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.BtnWrapper}>
        <Button BtnTxt="FINISH" onPress={() => onNext()} />
      </View>
      <View style={styles.BotomTxtWrapper}>
        <Text style={{ fontSize: rf(14), color: "#C90002" }}>
          Do you want to become a cooker and make money?
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  BtnWrapper: {
    height: hp("15%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
    alignItems: "center",
  },
  ProfilePicWrapper: {
    height: hp("30%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
    alignItems: "center",
  },
  TextWrapper: {
    height: hp("5%"),
    justifyContent: "center",
    paddingHorizontal: wp("5%"),
  },
  ProfilePicBody: {
    height: hp("25%"),
    width: hp("25%"),
    borderRadius: hp("25%") / 2,
    backgroundColor: "#C4C4C4",
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
  },
  BotomTxtWrapper: {
    width: wp("100%"),
    paddingHorizontal: wp("3%"),
    justifyContent: "center",
    alignItems: "center",
  },
  cameraIcon: {
    position: "absolute",
  },
});
