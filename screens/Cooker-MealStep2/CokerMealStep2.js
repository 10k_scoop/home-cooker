import React, { useState, useEffect } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  ActivityIndicator,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from "@expo/vector-icons";
import BottomMenu from "../../Components/BottomMenu";
import { connect } from "react-redux";
import { getUserOAuthToken } from "../../state-management/actions/auth/authActions";
import { Picker } from "@react-native-picker/picker";
import { TextInput } from "react-native-gesture-handler";
import * as ImagePicker from "expo-image-picker";
import { getCategories } from "../../state-management/actions/features/Actions";

const CokerMealStep2 = (props) => {
  const [image, setImage] = useState(null);
  const [mealPhoto, setMealPhoto] = useState(null);
  const [category, setCategory] = useState();
  const [quantity, setQuantity] = useState(1);
  const [gram, setGram] = useState("100g");
  const [timeForConsumption, setTimeForConsumption] = useState(1);
  const [price, setPrice] = useState(1);
  const [loading, setLoading] = useState(true);
  const [categoryList, setCategoryList] = useState([]);

  const pickImage = async () => {
    // No permissions request is necessary for launching the image library
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.IMAGE,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
      setMealPhoto(result);
    }
  };

  useEffect(() => {
    if (props.get_user_oauth_token != null) {
      props.getCategories(props.get_user_oauth_token.access_token, setLoading);
    }
  }, []);

  useEffect(() => {
    if (props.get_category != null) {
      setCategoryList(JSON.parse(props.get_category));
      setCategory(JSON.parse(props.get_category)[0].name);
    }
  }, [props]);

  const onNext = () => {
    if (
      price != "" &&
      quantity != "" &&
      gram != "" &&
      image != null &&
      category != ""
    ) {
      let data = {
        title: props.route.params.data.title,
        desc: props.route.params.data.desc,
        price: price,
        quantity: quantity,
        gram: gram,
        mealPhoto: mealPhoto,
        image: image,
        category: category,
        timeForConsumption: timeForConsumption,
      };
      props.navigation.navigate("CookerMealStep3", { data: data });
    } else {
      alert("Fill all details");
    }
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="black" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
      >
        <ScrollView>
          <View style={styles.innerWrapper}>
            {/* Header */}
            <View style={styles.Header}>
              <TouchableOpacity onPress={() => props.navigation.goBack()}>
                <AntDesign name="back" size={rf(18)} color="black" />
              </TouchableOpacity>
            </View>
            {loading ? (
              <ActivityIndicator size="large" color="black" />
            ) : (
              <>
                {/* Header */}
                <View style={styles.heading}>
                  <Text style={styles.headingText}>Location</Text>
                </View>
                {/* Category */}
                <View style={styles.categoryWrapper}>
                  <Text style={styles.categoryText}>category</Text>
                  <View style={styles.pickerView}>
                    <Picker
                      selectedValue={category}
                      onValueChange={(itemValue, itemIndex) =>
                        setCategory(itemValue)
                      }
                      itemStyle={{ fontSize: rf(13) }}
                    >
                      {categoryList.length > 1 ? (
                        categoryList.map((item, index) => {
                          return (
                            <Picker.Item
                              key={index}
                              label={item.name}
                              value={item.name}
                            />
                          );
                        })
                      ) : (
                        <Picker.Item label="loading...." value="loading...." />
                      )}
                    </Picker>
                  </View>
                </View>
                {/* category */}
                <View style={styles.QuantityGramWrapper}>
                  <View style={styles.quantity}>
                    <Text style={styles.quantityText}>Quantity</Text>
                    <View style={styles.quantityBox}>
                      <TextInput
                        style={styles.BoxFont}
                        placeholder="eg: 1"
                        keyboardType="number-pad"
                        onChangeText={(val) => setQuantity(val)}
                      />
                    </View>
                  </View>
                  <View style={styles.quantity}>
                    <Text style={styles.quantityText}>de Gramm</Text>
                    <View style={styles.quantityBox}>
                      <TextInput
                        style={styles.BoxFont}
                        placeholder="eg: 100g"
                        keyboardType="number-pad"
                        onChangeText={(val) => setGram(val)}
                      />
                    </View>
                  </View>
                </View>
                <View style={styles.priceWrapper}>
                  <Text style={styles.PriceText}>
                    Price for one item in Euros
                  </Text>
                  <View style={[styles.quantityBox, { width: "30%" }]}>
                    <TextInput
                      style={styles.BoxFont}
                      placeholder="eg: 100"
                      keyboardType="number-pad"
                      onChangeText={(val) => setPrice(val)}
                    />
                  </View>
                </View>

                <View style={styles.categoryWrapper}>
                  <Text style={styles.categoryText}>Time for consumption</Text>
                  <View style={styles.pickerView}>
                    <Picker
                      selectedValue={timeForConsumption}
                      onValueChange={(itemValue, itemIndex) =>
                        setTimeForConsumption(itemValue)
                      }
                      itemStyle={{ fontSize: rf(13) }}
                    >
                      <Picker.Item label="Select Hours" value="" />
                      <Picker.Item label="1" value="1" />
                      <Picker.Item label="2" value="2" />
                      <Picker.Item label="3" value="3" />
                      <Picker.Item label="4" value="4" />
                      <Picker.Item label="5" value="5" />
                      <Picker.Item label="6" value="6" />
                      <Picker.Item label="7" value="7" />
                      <Picker.Item label="8" value="8" />
                    </Picker>
                  </View>
                </View>

                <View style={styles.PhotoTitle}>
                  <Text style={styles.photoText}>Photo</Text>
                </View>
                <View style={styles.ImageWrapper}>
                  <TouchableOpacity style={styles.Img} onPress={pickImage}>
                    {image == null ? (
                      <Image
                        source={require("../../assets/MealPic.jpg")}
                        style={{ width: "100%", height: "100%" }}
                        resizeMode="cover"
                      />
                    ) : (
                      <Image
                        source={{ uri: image }}
                        style={{ width: "100%", height: "100%" }}
                        resizeMode="cover"
                      />
                    )}
                    <View style={styles.cameraBtnWrapper}>
                      <View>
                        <AntDesign name="camera" size={rf(40)} color="white" />
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={styles.BtnWrapper}>
                  <TouchableOpacity style={styles.Btn} onPress={onNext}>
                    <Text style={styles.BtnText}>Next</Text>
                  </TouchableOpacity>
                </View>
              </>
            )}
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Header: {
    width: wp("100%"),
    height: hp("10%"),
    justifyContent: "flex-end",
    paddingHorizontal: wp("2%"),
  },
  heading: {
    width: wp("90%"),
    height: hp("8%"),
    justifyContent: "center",
  },
  headingText: {
    fontSize: rf(26),
    fontWeight: "400",
    color: "#5E0D13",
  },
  categoryWrapper: {
    width: wp("90%"),
    height: hp("12%"),
    justifyContent: "space-around",
  },
  pickerView: {
    borderWidth: 1,
    paddingHorizontal: "5%",
    justifyContent: "center",
    height: hp("7%"),
    overflow: "hidden",
    marginTop: 10,
    borderColor: "#e5e5e5",
    borderRadius: 10,
    borderColor: "#5E0D13",
  },
  categoryText: {
    fontSize: rf(16),
    color: "#5E0D13",
  },
  QuantityGramWrapper: {
    width: wp("90%"),
    height: hp("12%"),
    flexDirection: "row",
    alignItems: "center",
  },
  quantity: {
    width: "40%",
    height: "90%",
    justifyContent: "space-around",
  },
  quantityBox: {
    width: "70%",
    height: "50%",
    borderWidth: 1.5,
    borderColor: "#5E0D13",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  Box: {
    width: "15%",
    height: "90%",
    justifyContent: "flex-end",
    paddingVertical: "2%",
    alignItems: "center",
  },
  quantityText: {
    fontSize: rf(16),
    color: "#5E0D13",
  },
  BoxFont: {
    fontSize: rf(15),
    color: "#5E0D13",
    fontWeight: "700",
  },
  priceWrapper: {
    width: wp("90%"),
    height: hp("12%"),
    justifyContent: "space-around",
  },
  PriceBox: {
    width: "18%",
    height: "60%",
    borderWidth: 1.5,
    borderColor: "#5E0D13",
    alignItems: "center",
    justifyContent: "center",
  },
  PriceText: {
    fontSize: rf(16),
    color: "#5E0D13",
  },
  PhotoTitle: {
    width: wp("90%"),
    marginVertical: hp("1%"),
  },
  photoText: {
    fontSize: rf(16),
    color: "#5E0D13",
  },
  ImageWrapper: {
    width: wp("90%"),
    height: hp("25%"),
    alignItems: "center",
    justifyContent: "center",
  },
  Img: {
    width: "100%",
    height: "90%",
    overflow: "hidden",
    borderRadius: 10,
  },
  BtnWrapper: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: hp("9%"),
  },
  Btn: {
    width: wp("90%"),
    height: hp("7%"),
    borderRadius: 10,
    backgroundColor: "#5E0D13",
    alignItems: "center",
    justifyContent: "center",
  },
  BtnText: {
    fontSize: rf(14),
    fontWeight: "700",
    color: "#fff",
  },
  cameraBtnWrapper: {
    width: "100%",
    height: "100%",
    backgroundColor: "#222",
    opacity: 0.5,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
  innerWrapper:{
    flex:1,
    alignItems:'center',
    height:hp('100%')
  }
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_oauth_token: state.main.get_user_oauth_token,
  get_user_details: state.main.get_user_details,
  get_category: state.main.get_category,
});
export default connect(mapStateToProps, { getUserOAuthToken, getCategories })(
  CokerMealStep2
);
