import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import BottomMenu from "../../Components/BottomMenu";
import { EvilIcons } from "@expo/vector-icons";

export default function CustomerAddress2({ navigation }) {
  return (
    <View style={styles.Container}>
      <View style={styles.title}>
        <Text style={styles.titleText}>Mes adresses</Text>
      </View>
      <View style={styles.SearchBar}>
        <View style={styles.SearchBox}>
          <EvilIcons name="search" size={24} color="#5E0D13" />
          <TextInput
            style={styles.SearchTextField}
            placeholder="Rechercher une adresse"
            placeholderTextColor="#5E0D13"
          />
        </View>
      </View>
      <View style={styles.TextFieldWrapper}>
        <Text style={styles.font}>Numéro fixe</Text>
        <TextInput style={styles.TextField} />
      </View>
      <View style={styles.TextFieldWrapper}>
        <Text style={styles.font}>Numéro portable</Text>
        <TextInput style={styles.TextField} />
      </View>
      <View style={styles.BtnWrapper}>
        <TouchableOpacity style={styles.Btn}>
          <Text style={styles.BtnText}>AJOUTER</Text>
        </TouchableOpacity>
      </View>
      <BottomMenu />
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  title: {
    width: wp("90%"),
    height: hp("15%"),
    justifyContent: "flex-end",
    paddingVertical: hp("2%"),
  },
  titleText: {
    fontSize: rf(26),
    fontWeight: "400",
    color: "#5E0D13",
  },
  SearchBar: {
    width: wp("90%"),
    height: hp("15%"),
    justifyContent: "center",
  },
  SearchBox: {
    width: "100%",
    height: "40%",
    borderWidth: 1,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#e5e5e5",
    borderRadius: 5,
    borderColor: "#5E0D13",
    paddingHorizontal: "1%",
  },
  SearchTextField: {
    width: "90%",
    height: "100%",
    fontSize: rf(16),
    paddingHorizontal: 10,
  },
  TextFieldWrapper: {
    width: wp("90%"),
    height: hp("10%"),
    justifyContent: "space-around",
    marginBottom: "5%",
  },
  TextField: {
    width: "100%",
    height: "60%",
    borderWidth: 1,
    borderColor: "#5E0D13",
    borderRadius: 5,
    paddingHorizontal: "3%",
  },
  font: {
    fontSize: rf(14),
    color: "#5E0D13",
  },
  BtnWrapper: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: hp("12%"),
  },
  Btn: {
    width: wp("18%"),
    height: hp("5%"),
    borderWidth: 1.5,
    borderRadius: 6,
    borderColor: "#5E0D13",
    backgroundColor: "#FF1E00",
    alignItems: "center",
    justifyContent: "center",
  },
  BtnText: {
    fontSize: rf(12),
    fontWeight: "700",
    color: "#fff",
  },
});
