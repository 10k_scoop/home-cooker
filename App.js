import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { AppNavigator } from "./routes/AppNavigator";
import { Provider } from "react-redux";
import store from "./state-management/store";
export default function App() {
  return (
    <Provider store={store}>
      <View style={styles.container}>
        <AppNavigator />
      </View>
    </Provider>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
