//Auth &&  Auth Tokens
export const GET_OAUTH_TOKEN = "GET_OAUTH_TOKEN";
export const GET_USER_OAUTH_TOKEN = "GET_USER_OAUTH_TOKEN";
export const DO_LOGOUT = "DO_LOGOUT";
export const DO_SIGNUP = "DO_SIGNUP";


//User Info
export const GET_USER_DETAILS = "GET_USER_DETAILS";
export const UPDATE_USER = "UPDATE_USER";

//Ratings
export const RATE_USER = "RATE_USER";
export const GET_USER_REVIEW = "GET_USER_REVIEW";

//Category
export const CREATE_CATEGORY = "CREATE_CATEGORY";
export const GET_CATEGORY = "GET_CATEGORY";


//Meals
export const CREATE_MEAL="CREATE_MEAL"
export const GET_MEALS_BY_LOCATION="GET_MEALS_BY_LOCATION"
export const GET_MEALS_BY_REFERENCE="GET_MEALS_BY_REFERENCE"
export const GET_MEALS_BY_COOKER="GET_MEALS_BY_COOKER"
//Errors
export const GET_ERRORS = "GET_ERRORS";