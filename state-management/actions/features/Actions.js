import {
  GET_ERRORS,
  DO_SIGNUP,
  GET_USER_DETAILS,
  UPDATE_USER,
  RATE_USER,
  GET_USER_REVIEW,
  GET_MEALS_BY_LOCATION,
  GET_CATEGORY,
  GET_MEALS_BY_COOKER,
  GET_MEALS_BY_REFERENCE,
} from "../../types/types";
import { decode as atob, encode as btoa } from "base-64";
const baseUrl = "https://allcooks-backend.herokuapp.com";

//Create a new user
export const createUser =
  (data, setLoading, setResponseCode, requestData) => async (dispatch) => {
    const Token = data.oAuthToken;
    let imageIndex = data.profileImg?.uri?.lastIndexOf("/");
    let imageName = data.profileImg?.uri?.slice(imageIndex + 1);
    var d = JSON.stringify(data);
    var form = new FormData();
    form.append("request", JSON.stringify(requestData));
    form.append("profileImg", {
      uri: data.profileImg.uri,
      name: "myimage.png",
      fileName: "image",
      type: "image/png",
    });

    try {
      fetch("https://allcooks-backend.herokuapp.com/users", {
        method: "POST",
        redirect: "follow",
        headers: {
          Authorization: "Bearer " + Token,
          "Content-Type":
            "multipart/form-data; charset=utf-8; boundary=------random-boundary",
        },
        body: form,
      })
        .then((response) => {
          const statusCode = response.status;
          setResponseCode(statusCode);
        })
        .then((json) => {
          setLoading(false);
          console.log(json);
          dispatch({ type: DO_SIGNUP, payload: { res: json } });
        })
        .catch((error) => {
          console.log(error);
          dispatch({ type: GET_ERRORS, payload: error.message });
          setLoading(false);
        });
    } catch (e) {
      dispatch({ type: GET_ERRORS, payload: e.message });
      setLoading(false);
      console.error(e.message);
    }
  };

//Get user's information
export const getUser = (token, data, setLoading) => async (dispatch) => {
  try {
    fetch(`${baseUrl}` + "/users/" + data.username, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.text())
      .then((json) => {
        console.log(json)
        setLoading(false);
        dispatch({ type: GET_USER_DETAILS, payload: json });
      })
      .catch((error) => {
        dispatch({ type: GET_ERRORS, payload: error.message });
        console.error(error.message);
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};

//Update User profile || Info
export const updateUser = (token, data, setLoading) => async (dispatch) => {
  try {
    fetch(`${baseUrl}` + "/users/", {
      method: "PUT",
      headers: {
        Authorization: "Bearer Token" + btoa(`${token}`),
      },
      body: {
        username: "CLS",
        password: "cls12",
        email: "lothfy@ldss.com",
        phoneNumber: "0685963214",
        sex: "MALE",
        profil: "CUSTOMER",
        address: {
          number: "17",
          street: "baudottes",
          country: "France",
          state: null,
          zipCode: 93000,
        },
        totalRating: {
          totalComments: 0,
          totalRatings: 1,
          avgScore: 0.0,
        },
        meals: null,
        favoriteMeals: null,
      },
    })
      .then((response) => response.text())
      .then((json) => {
        setLoading(false);
        dispatch({ type: UPDATE_USER, payload: json });
      })
      .catch((error) => {
        dispatch({ type: GET_ERRORS, payload: error.message });
        console.error(error);
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

// Rating a user || Comments
export const rateUser = (token, data, setLoading) => async (dispatch) => {
  try {
    fetch(`${baseUrl}` + "/users/rate/CLS", {
      method: "POST",
      headers: {
        Authorization: "Bearer Token" + btoa(`${token}`),
      },
      body: {
        comment: "test",
        score: 5,
      },
    })
      .then((response) => response.text())

      .then((json) => {
        setLoading(false);
        dispatch({ type: RATE_USER, payload: json });
      })
      .catch((error) => {
        dispatch({ type: GET_ERRORS, payload: error.message });
        console.error(error);
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

// Order a meal
export const OrderMeal =
  (token, data, setLoading, setResponseCode) => async (dispatch) => {
    console.log(data);
    try {
      fetch(`${baseUrl}` + "/orders", {
        method: "POST",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          instanceMealReference: data.instanceMealReference,
          shopper: data.shopper,
          quantity: data.quantity,
        }),
      })
        .then((response) => {
          const statusCode = response.status;
          setResponseCode(statusCode);
        })
        .then((json) => {
          setLoading(false);
        })
        .catch((error) => {
          dispatch({ type: GET_ERRORS, payload: error.message });
          console.log(error);
          setLoading(false);
        });
    } catch (e) {
      dispatch({ type: GET_ERRORS, payload: e.message });
      setLoading(false);
    }
  };

//Remove a meal
export const removeMeal =
  (token, reference, setLoading, setMealRemove) => async (dispatch) => {
    console.log(token);
    console.log(reference);
    try {
      fetch(`${baseUrl}` + "/instanceMeals/disable/" + reference, {
        method: "POST",
        headers: {
          Authorization: "Bearer Token" + btoa(`${token}`),
        },
      })
        .then((response) => response.text())
        .then((json) => {
          setLoading(false);
          console.log(json);
          setMealRemove(true);
        })
        .catch((error) => {
          dispatch({ type: GET_ERRORS, payload: error.message });
          console.log(error.message);
          setLoading(false);
          setMealRemove(false);
        });
    } catch (e) {
      console.log(e.message);
      dispatch({ type: GET_ERRORS, payload: e.message });
      setLoading(false);
      setMealRemove(false);
    }
  };

//Get users rating
export const getUserRatings = (token, data, setLoading) => async (dispatch) => {
  try {
    fetch(`${baseUrl}` + "/users/rate/CLS", {
      method: "GET",
      headers: {
        Authorization: "Bearer Token" + btoa(`${token}`),
      },
    })
      .then((response) => response.text())
      .then((json) => {
        setLoading(false);
        dispatch({ type: GET_USER_REVIEW, payload: json });
      })
      .catch((error) => {
        dispatch({ type: GET_ERRORS, payload: error.message });
        console.error(error);
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

//Get Meals by location
export const getMealsByLocation =
  (token, data, setLoading) => async (dispatch) => {
    try {
      fetch(`${baseUrl}` + "/instanceMeals?longitude=50&latitude=3", {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
        },
      })
        .then((response) => response.text())
        .then((json) => {
          setLoading(false);
          dispatch({ type: GET_MEALS_BY_LOCATION, payload: json });
        })
        .catch((error) => {
          dispatch({ type: GET_ERRORS, payload: error.message });
          console.error(error);
          setLoading(false);
        });
    } catch (e) {
      dispatch({ type: GET_ERRORS, payload: e.message });
      setLoading(false);
    }
  };

//Get Categories
export const getCategories = (token, setLoading) => async (dispatch) => {
  try {
    fetch(`${baseUrl}` + "/categories", {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.text())
      .then((json) => {
        setLoading(false);
        dispatch({ type: GET_CATEGORY, payload: json });
      })
      .catch((error) => {
        dispatch({ type: GET_ERRORS, payload: error.message });
        console.error(error);
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

//Create Meal
export const createMeal =
  (data, token, setLoading, setResponseCode,images) => async (dispatch) => {
    const Token = token;
    let formData = new FormData();
    formData.append("request", JSON.stringify(data));
    formData.append("images", {
      uri: images?.uri,
      name: "myimage.png",
      fileName: "image",
      type: "image/png",
    });
    
    try {
      fetch(`${baseUrl}` + "/instanceMeals", {
        method: "POST",
        headers: {
          Authorization: "Bearer " + Token,
          "Content-Type":
            "multipart/form-data; boundary=----WebKitFormBoundaryIn312MOjBWdkffIM",
        },
        body: formData,
      })
        .then((response) => {
          const statusCode = response.status;
          console.log(statusCode);
          setResponseCode(statusCode);
        })
        .then((json) => {
          setLoading(false);
          console.log(json);
        })
        .catch((error) => {
          console.log(error);
          console.log("Error aw bha");
          dispatch({ type: GET_ERRORS, payload: error.message });
          setLoading(false);
        });
    } catch (e) {
      dispatch({ type: GET_ERRORS, payload: e.message });
      setLoading(false);
      console.error(e.message);
    }
  };

//Get Cooker meals bycooker
export const getMealsByCooker = (token, setLoading) => async (dispatch) => {
  try {
    fetch(`${baseUrl}` + "/instanceMeals/bycooker", {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.text())
      .then((json) => {
        setLoading(false);
        dispatch({ type: GET_MEALS_BY_COOKER, payload: json });
      })
      .catch((error) => {
        dispatch({ type: GET_ERRORS, payload: error.message });
        console.log(error);
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

//Get Cooker meals by referrence
export const getMealsByReferrence =
  (token, ref, setLoading) => async (dispatch) => {
    try {
      fetch(`${baseUrl}` + "/instanceMeals/" + ref, {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
        },
      })
        .then((response) => response.text())
        .then((json) => {
          setLoading(false);
          dispatch({ type: GET_MEALS_BY_REFERENCE, payload: json });
        })
        .catch((error) => {
          dispatch({ type: GET_ERRORS, payload: error.message });
          console.log(error);
          setLoading(false);
        });
    } catch (e) {
      dispatch({ type: GET_ERRORS, payload: e.message });
      setLoading(false);
    }
  };

//Accept Order
export const acceptOrder =
  (token, reference, setLoading) => async (dispatch) => {
    try {
      fetch(`${baseUrl}` + "/orders/validate/" + reference, {
        method: "POST",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
        },
      })
        .then((response) =>response.text())
        .then((json) => {
          console.log(json);
        })
        .catch((error) => {
          dispatch({ type: GET_ERRORS, payload: error.message });
          console.log(error.message);
          setLoading(false);
        });
    } catch (e) {
      console.log(e.message);
      dispatch({ type: GET_ERRORS, payload: e.message });
      setLoading(false);
    }
  };

//Cancel Order
export const cancelOrder =
  (token, reference, setLoading) => async (dispatch) => {
    console.log(reference);
    try {
      fetch(`${baseUrl}` + "/orders/cancel/" + reference, {
        method: "POST",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
        },
      })
        .then((response) => {
          const statusCode = response.status;
          console.log(statusCode);
        })
        .then((json) => {
          console.log(json);
          setLoading(false);
        })
        .catch((error) => {
          dispatch({ type: GET_ERRORS, payload: error.message });
          console.log(error.message);
          setLoading(false);
        });
    } catch (e) {
      console.log(e.message);
      dispatch({ type: GET_ERRORS, payload: e.message });
      setLoading(false);
    }
  };
