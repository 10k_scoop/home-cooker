import {
  GET_ERRORS,
  DO_LOGOUT,
  GET_OAUTH_TOKEN,
  GET_USER_OAUTH_TOKEN,
  GET_USER_DETAILS,
} from "../../types/types";
import { decode as atob, encode as btoa } from "base-64";
import { AsyncStorage } from "react-native";
const baseUrlOAuth = "https://allcooks-oauth-server.herokuapp.com";

const baseUrl = "https://allcooks-backend.herokuapp.com";

const username = "allCooks-client";
const password = "allCooks-secret";

export const getOAuthToken = (setLoading) => async (dispatch) => {
  setLoading(true);
  let formdata = new FormData();
  formdata.append("grant_type", "client_credentials");
  try {
    fetch(`${baseUrlOAuth}` + "/oauth/token", {
      method: "POST",
      headers: {
        Authorization: "Basic " + btoa(`${username}:${password}`),
      },
      body: formdata,
    })
      .then((response) => response.json())
      .then((json) => {
        setLoading(false);
        dispatch({ type: GET_OAUTH_TOKEN, payload: json });
      })
      .catch((error) => {
        dispatch({ type: GET_ERRORS, payload: error.message });
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

//Get user data from Customer or Cooker auth token as local function
export const getUserOAuthToken = (data, setLoading) => async (dispatch) => {
  let formdata = new FormData();
  formdata.append("grant_type", "password");
  formdata.append("username", data.username);
  formdata.append("password", data.password);

  try {
    fetch(`${baseUrlOAuth}` + "/oauth/token", {
      method: "POST",
      headers: {
        Authorization: "Basic " + btoa(`${username}:${password}`),
      },
      body: formdata,
    })
      .then((response) => response.text())
      .then((json) => {
        setLoading(false)
        dispatch({ type: GET_USER_OAUTH_TOKEN, payload: JSON.parse(json) });
      })
      .catch((error) => {
        dispatch({ type: GET_ERRORS, payload: error.message });
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

export const logout = (setLoading) => async (dispatch) => {
  try {
    console.log("logeed out");
    await AsyncStorage.removeItem("userData");
    await AsyncStorage.removeItem("userSessionToken");
    dispatch({ type: GET_USER_OAUTH_TOKEN, payload: { access_token: null } });
    dispatch({ type: GET_USER_DETAILS, payload: null});
    dispatch({ type: DO_LOGOUT, payload: "success" });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};
