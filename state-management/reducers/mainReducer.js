import {
  DO_LOGOUT,
  DO_SIGNUP,
  GET_ERRORS,
  GET_OAUTH_TOKEN,
  GET_USER_OAUTH_TOKEN,
  GET_USER_DETAILS,
  UPDATE_USER,
  RATE_USER,
  GET_USER_REVIEW,
  CREATE_CATEGORY,
  GET_CATEGORY,
  CREATE_MEAL,
  GET_MEALS_BY_LOCATION,
  GET_MEALS_BY_COOKER,
  GET_MEALS_BY_REFERENCE
} from "../types/types";
const initialState = {
  logout: null,
  signup_res: null,
  errors: null,
  get_oauth_token: null,
  get_user_oauth_token: null,
  get_user_details: null,
  update_user: null,
  rate_user: null,
  get_user_review: null,
  create_category: null,
  get_category: null,
  create_meal: null,
  get_meals_by_location: null,
  get_meals_by_cooker: null,
  get_meals_by_reference:null
};
const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case DO_LOGOUT:
      return {
        ...state,
        logout: action.payload,
      };
    case DO_SIGNUP:
      return {
        ...state,
        signup_res: action.payload,
      };
    case GET_ERRORS:
      return {
        ...state,
        errors: action.payload,
      };
    case GET_OAUTH_TOKEN:
      return {
        ...state,
        get_oauth_token: action.payload,
      };
    case GET_USER_OAUTH_TOKEN:
      return {
        ...state,
        get_user_oauth_token: action.payload,
      };
    case GET_USER_DETAILS:
      return {
        ...state,
        get_user_details: action.payload,
      };
    case UPDATE_USER:
      return {
        ...state,
        update_user: action.payload,
      };
    case RATE_USER:
      return {
        ...state,
        rate_user: action.payload,
      };

    case GET_USER_REVIEW:
      return {
        ...state,
        get_user_review: action.payload,
      };
    case CREATE_CATEGORY:
      return {
        ...state,
        create_category: action.payload,
      };
    case GET_CATEGORY:
      return {
        ...state,
        get_category: action.payload,
      };
    case CREATE_MEAL:
      return {
        ...state,
        create_meal: action.payload,
      };
    case GET_MEALS_BY_LOCATION:
      return {
        ...state,
        get_meals_by_location: action.payload,
      };
    case GET_MEALS_BY_COOKER:
      return {
        ...state,
        get_meals_by_cooker: action.payload,
      };
    case GET_MEALS_BY_REFERENCE:
      return {
        ...state,
        get_meals_by_reference: action.payload,
      };
    default:
      return state;
  }
};
export default mainReducer;
