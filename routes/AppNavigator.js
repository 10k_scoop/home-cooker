import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import MealList from "../screens/MealList";
import MealDetail from "../screens/Meal-Detail/MealDetail";
import LoggedOut from "../screens/LoggedOut";
import Register1 from "../screens/Register1/Register1";
import RegisterBasicInfo from "../screens/RegisterBasicInfo/RegisterBasicInfo";
import RegisterLocation from "../screens/RegisterLocation/RegisterLocation";
import RegisterProfile from "../screens/RegisterProfile/RegisterProfile";
import Login from "../screens/Login/Login";
import CustomerRegulation from "../screens/Customer-Regulation/CustomerRegulation";
import CookerInformation from "../screens/Cooker-Information/CookerInformation";
import CookerLocation from "../screens/Cooker-Location/CookerLocation";
import CookerExperience from "../screens/Cooker-Experience/CookerExperience";
import CookerProfile from "../screens/Cooker-Profile/CookerProfile";
import CookerRegulation from "../screens/Cooker-Regulation/CookerRegulation";
import ChooseOption from "../screens/Choose-Option/ChooseOption";
import CookerRegister from "../screens/Cooker-Register/CookerRegister";
import CustomerCart from "../screens/Customer-Cart/CustomerCart";
import CustomerEmptyCart from "../screens/Customer-EmptyCart/CustomerEmptyCart";
import CustomerCBPayment from "../screens/Customer-CBPayment/CustomerCBPayment";
import CustomerPaypal from "../screens/CustomerPaypal/CustomerPaypal";
import CustomerComfirmation from "../screens/CustomerComfirmation/CustomerComfirmation";
import CustomerDashboard from "../screens/Customer-Dashboard/CustomerDashboard";
import APropos from "../screens/A-propos/APropos";
import CustomerFAQ from "../screens/Customer-FAQ/CustomerFAQ";
import CustomerInviter from "../screens/Customer-Inviter/CustomerInviter";
import CustomerCredit from "../screens/Customer-Credit/CustomerCredit";
import CustomerAddress1 from "../screens/Customer-Address1/CustomerAddress1";
import CustomerAddress2 from "../screens/Customer-Address2/CustomerAddress2";
import CustomerPaiement from "../screens/Customer-paiement/CustomerPaiement";
import CustomerCB from "../screens/Customer-CB/CustomerCB";
import CustomerInformation from "../screens/Customer-Information/CustomerInformation";
import CustomerCommandes from "../screens/Customer-Commandes/CustomerCommandes";
import CookerMealOrder from "../screens/Cooker-MealOrder/CookerMealOrder";
import CookerMealStep1 from "../screens/Cooker-MealStep1/CookerMealStep1";
import CookerMealStep2 from "../screens/Cooker-MealStep2/CokerMealStep2";
import CookerMealStep3 from "../screens/Cooker-MealStep3/CookerMealStep3";
import CookerDashboard from "../screens/Cooker-Dashboard/CookerDashboard";
import ForgetPassword from "../screens/Forget-Password/ForgetPassword";
import CookerMeal from "../screens/CookerMeal/CookerMeal";
import PaymentMethod from "../screens/Payment-Method/PaymentMethod";

const { Navigator, Screen } = createStackNavigator();

function AppNavigation() {
  return (
    <Navigator screenOptions={{ headerShown: false }}>
      <Screen name="ChooseOption" component={ChooseOption} />
      <Screen name="LoggedOut" component={LoggedOut} />
      <Screen name="Register1" component={Register1} />
      <Screen name="PaymentMethod" component={PaymentMethod} />
      <Screen name="RegisterBasicInfo" component={RegisterBasicInfo} />
      <Screen name="RegisterLocation" component={RegisterLocation} />
      <Screen name="RegisterProfile" component={RegisterProfile} />
      <Screen name="Login" component={Login} />
      <Screen name="CustomerRegulation" component={CustomerRegulation} />
      <Screen name="CookerRegister" component={CookerRegister} />
      <Screen name="CookerInformation" component={CookerInformation} />
      <Screen name="CookerLocation" component={CookerLocation} />
      <Screen name="CookerExperience" component={CookerExperience} />
      <Screen name="CookerProfile" component={CookerProfile} />
      <Screen name="CookerRegulation" component={CookerRegulation} />
      <Screen name="MealList" component={MealList} />
      <Screen name="MealDetail" component={MealDetail} />
      <Screen name="CustomerCart" component={CustomerCart} />
      <Screen name="CustomerEmptyCart" component={CustomerEmptyCart} />
      <Screen name="CustomerCBPayment" component={CustomerCBPayment} />
      <Screen name="CustomerPaypal" component={CustomerPaypal} />
      <Screen name="CustomerComfirmation" component={CustomerComfirmation} />
      <Screen name="CustomerDashboard" component={CustomerDashboard} />
      <Screen name="APropos" component={APropos} />
      <Screen name="CustomerFAQ" component={CustomerFAQ} />
      <Screen name="CustomerInviter" component={CustomerInviter} />
      <Screen name="CustomerCredit" component={CustomerCredit} />
      <Screen name="CustomerAddress1" component={CustomerAddress1} />
      <Screen name="CustomerAddress2" component={CustomerAddress2} />
      <Screen name="CustomerPaiement" component={CustomerPaiement} />
      <Screen name="CustomerCB" component={CustomerCB} />
      <Screen name="CustomerInformation" component={CustomerInformation} />
      <Screen name="CustomerCommandes" component={CustomerCommandes} />
      <Screen name="CookerMealOrder" component={CookerMealOrder} />
      <Screen name="CookerMealStep1" component={CookerMealStep1} />
      <Screen name="CookerMealStep2" component={CookerMealStep2} />
      <Screen name="CookerMealStep3" component={CookerMealStep3} />
      <Screen name="CookerDashboard" component={CookerDashboard} />
      <Screen name="CookerMeal" component={CookerMeal} />
      <Screen name="ForgetPassword" component={ForgetPassword} />
    </Navigator>
  );
}
export const AppNavigator = () => (
  <NavigationContainer>
    <AppNavigation />
  </NavigationContainer>
);
