import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import ChooseOption from "../screens/Choose-Option/ChooseOption";
import CookerRegister from "../screens/Cooker-Register/CookerRegister";
import CustomerRegulation from "../screens/Customer-Regulation/CustomerRegulation";
import CookerInformation from "../screens/Cooker-Information/CookerInformation";
import CookerLocation from "../screens/Cooker-Location/CookerLocation";
import CookerExperience from "../screens/Cooker-Experience/CookerExperience";
import CookerProfile from "../screens/Cooker-Profile/CookerProfile";
import CookerRegulation from "../screens/Cooker-Regulation/CookerRegulation";
import ForgetPassword from "../screens/Forget-Password/ForgetPassword";
import Register1 from "../screens/Register1/Register1";
import RegisterBasicInfo from "../screens/RegisterBasicInfo/RegisterBasicInfo";
import RegisterLocation from "../screens/RegisterLocation/RegisterLocation";
import RegisterProfile from "../screens/RegisterProfile/RegisterProfile";
import LoggedOut from "../screens/LoggedOut";
import Login from "../screens/Login/Login";
const { Navigator, Screen } = createStackNavigator();

function AppNavigation() {
  return (
    <Navigator screenOptions={{ headerShown: false }}>
      <Screen name="ChooseOption" component={ChooseOption} />
      <Screen name="LoggedOut" component={LoggedOut} />
      <Screen name="Login" component={Login} />
      <Screen name="Register1" component={Register1} />
      <Screen name="RegisterBasicInfo" component={RegisterBasicInfo} />
      <Screen name="RegisterLocation" component={RegisterLocation} />
      <Screen name="RegisterProfile" component={RegisterProfile} />
      <Screen name="CustomerRegulation" component={CustomerRegulation} />
      <Screen name="CookerRegister" component={CookerRegister} />
      <Screen name="CookerInformation" component={CookerInformation} />
      <Screen name="CookerLocation" component={CookerLocation} />
      <Screen name="CookerExperience" component={CookerExperience} />
      <Screen name="CookerProfile" component={CookerProfile} />
      <Screen name="CookerRegulation" component={CookerRegulation} />
      <Screen name="ForgetPassword" component={ForgetPassword} />
    </Navigator>
  );
}
export const UnAuthNavigator = () => (
  <NavigationContainer>
    <AppNavigation />
  </NavigationContainer>
);
